# AI
- Easy to tell when to fire it; enemy slot selected in battlefield
- In theory you could go super-dynamic and have a "player ai" that prompts you to select a target while others do it for you
	- But this would impact unit configuration
- Typically all units have an AI
	- Start wihtout a dedicated service; early code will be rather short
- When selecting an enemy slot, the battlefield will call unit.executeAI instead of prompting for targets and listing abilities
- Slot racks will handle basic getters, like getTank, getExposed, getAdjascent, etc.
- AI has an execute() (or other name to not impact ability) function
	- This will select targets and abilities based on criteria and fire
	- All AI should inherit some default that picks the first available enemy if all else fails
- A tricky part is knowing what abilities are available but for starters we could just assume they exist

# Tank logic
- First and foremost: A tank cannot be exposed unless replaced.
	- Problem is that if we forbid the tank from expose() we cannot have special ability cases that replace thank and expose
	- technically we could have a tankExpose() function to be safe but since abilities shouldn't be able to fire at all if tank is tank this might be overkill
	- HOWEVER consider an ability that just plain exposes all enemy slots.
		- Ability could have a safeExpose() function that checks before exposing also
- When a tank is exposed, normalized, killed etc. a new tank must emerge
	- Simplest way is to call getParent() replaceTank() straight from the slot itself when an intrusive action happens
	- A tankDisabled() function in parent is however a little more robust as it can contain more than just replacement logic

# Decals
- Give slots little icons that represent the current state (shield for tank, ! for exposed etc)
- Should be simple enough to just add them to slot and work from there 

# Dynamic abilities
- Would be a neat addition; imagine effect-objects that you collect and execute in order
- example: Ability that burns all exposed and exposes self
	- Effect: DamageAllExposed
		- Requirement: At least one exposed enemy or ally
		- Targets: all	
		- Required: yes
	- Effect: ExposeSelf
		- Requirement: none
		- Required: no
		- Targets: self
- Ability.getTargets()
	- For each effect: get targets
		- If < 1 and effect required, return nothing and prevent cast
		- If not, add to list of targets and check next
		- If all abilities pass, set last fetched targets and return array
	- Conditions are still not separate; there is potential for repeat code in that sense 
		- Still, I don't think this game will ever ever get big enough to give thousands of line of condition code alone 
			- I doubt even big ass games have all that much
- Making a new ability is still a bit wonky
	- This is the kind of stuff you'll want when things start piling up
	- HOWEVER it's easier to make a robust system now than to do it later
- Make AI, UI, and other "demo-able state" things first. Maybe have 3-4 abilities that you can later rework.

# UI improvements
- An ability panel
- A selected unit data panel
- Ability tooltips
- An empty top bar for later resource/metadata listing

# Ability improvements
- Passives that trigger on select()
- Non-pick-target abilities that show a confirmation dialog
- Limited-cast abilities, e.g. 2/3 uses left kinda shit

# Items 
- Still far off in scope
- Basically start with consumables
- Unit
	-> Inventory
		-> Consumables
			-> item, item, item
- Example flow:
	1. Another button, "use item...", next to abilities on select
	2. A small popup window showing your inventory (showInventorySmall())
	3. Clicking the item makes it act as an ability; it will either fire right away (confirmation) or let you choose a target
	4. Ability is removed on use

# Stat logic
- Some overall stat system (damage blocking, resistance, power increase)
- Gear and unit stats work in tandem
- Unit.getPower(Ability, baseAmount)
	- Basically does all the needed calculations
	- Then on to enemy.getImpact(Ability, power)
- Some gear could grant abilities as well
	- Could have children of active/passive abilities which are loaded along with unit regular abilities


