extends Node2D

class_name ConversationNode
# Core class for conversation chains

export var title = "N/A"
export var message = "N/A"
export var from_player = false
export var repeat_options = false # Not implemented, but would be nice to add parent options again
onready var portrait = _get_portrait()

func load() -> void:
	$ConversationNodeEmitter.loaded()
	# Emits signal noting it's been loaded 
	# Called when parent is selected
	# Standard conversation snippets should ideally appear as title-named buttons linked to select()
	# Conditions should instead just pass-through
	
func select() -> void:
	$ConversationNodeEmitter.selected()
	
	# Call load for all children, preparing continuation of chain (e.g. buttons of choices)
	var _loadable_children = 0
	for conversation_node in get_children():
		if (conversation_node.has_method("load")):
			conversation_node.load()
			_loadable_children += 1
			
	if _loadable_children == 0:
		$ConversationNodeEmitter.ended_conversation()
		print("Ending conversation")
		
	# If get_children empty, exit the conversation
	# Possibly wrap in a conversation object that can yield some effect on ext
		
	# Emit signal of selection 
		# This tells UI to display the message, speaker portrait and name, etc.
		
func _get_portrait():
	return $Portrait.texture
	
