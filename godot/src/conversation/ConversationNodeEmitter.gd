extends Node2D

class_name ConversationNodeEmitter

var _listener = "conversation_node_listeners"

func selected():
	get_tree().call_group(_listener, "selected", get_parent())
	
func loaded():
	get_tree().call_group(_listener, "loaded", get_parent())
	
func ended_conversation():
	get_tree().call_group(_listener, "ended_conversation", get_parent())
