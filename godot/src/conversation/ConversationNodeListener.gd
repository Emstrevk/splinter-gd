extends Node2D

class_name ConversationNodeListener

signal selected
signal loaded
signal ended_conversation

func _ready():
	safe_connect("selected", "conversation_node_selected")
	safe_connect("loaded", "conversation_node_loaded")
	safe_connect("ended_conversation", "conversation_node_ended_conversation")
	
	add_to_group("conversation_node_listeners")

func safe_connect(listener_signal, parent_function):
	if (get_parent().has_method(parent_function)):
		connect(listener_signal, get_parent(), parent_function)
		
func selected(conversation_node):
	emit_signal("selected", conversation_node)
	
func loaded(conversation_node):
	emit_signal("loaded", conversation_node)
	
func ended_conversation(conversation_node):
	emit_signal("ended_conversation", conversation_node)
