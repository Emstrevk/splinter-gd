extends Node2D

# Ideally none of these should be in use.
class_name Services

onready var GUI = $"/root/Game/GUI"
onready var PLAYER = $"/root/Game/PLAYER_SERVICE"
onready var BATTLE = $"/root/Game/BATTLE_SERVICE"
onready var ZONE = $"/root/Game/ZONE_SERVICE"
onready var GAME = $"/root/Game"

func _ready() -> void:
	self.name = "Services"
