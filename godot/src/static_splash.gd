extends Node2D

# Is this in use? Should it be?
class_name StaticSplash

onready var tween_node = get_node("Tween")
onready var sprite_node = get_node("Sprite")
onready var animation_player = get_node("AnimationPlayer")

var animation_time = 0.5

var idleAnimation: Animation
var text_label: RichTextLabel


# TOdo: REwork completely using slotAnimator and simple_splash
func _ready():

	# Tween scale to give "popup" effect
	var inflated_scale = Vector2(get_child(0).scale.x * 2, get_child(0).scale.y * 2)
	tween_node.interpolate_property(get_child(0), "scale", get_child(0).scale, inflated_scale, animation_time, Tween.TRANS_BOUNCE, Tween.EASE_OUT, 0)
	tween_node.interpolate_property(get_child(0), "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), animation_time * 3, Tween.TRANS_QUAD, Tween.EASE_IN, 0)
	tween_node.start()

	idleAnimation = Animation.new()
	idleAnimation.set_length(animation_time * 4)
	animation_player.add_animation("idleAnimation", idleAnimation)
	animation_player.play("idleAnimation")
	animation_player.connect("animation_finished", self, "_animation_finished")
	text_label = get_child(0).get_child(0) # A bit clumsy but all we need for now
	text_label.text = ""

func _process(delta):
	sprite_node.position = get_parent().position

func label(message: String):
	text_label.text = message

func _animation_finished(anim_name):
	get_parent().remove_child(self)

func set_position(position):
	get_child(0).position = position
