extends Control

class_name Unit

#var _demo_name_list = ["Dips"]
var battlecry_list = ["Protec!", "Grr!", "Hiss!", "Mau!"]
#var _name = _demo_name_list[randi() % _demo_name_list.size()]
export var _max_hp : float
onready var _current_hp : float = _max_hp

#onready var portrait : StreamTexture = load("res://graphics/debug_portrait.png")

#signal return_to_container

func _ready() -> void:
	
	var name_label:Label = Label.new()
	name_label.name = "NameLabel"
	name_label.rect_position = Vector2(-100, -50)
	name_label.rect_size = Vector2(161,42)
	name_label.margin_left = -100
	name_label.margin_top = -50
	name_label.margin_bottom = -5
	name_label.margin_right = 60
	name_label.align = HALIGN_CENTER
	name_label.set("custom_fonts/font", load("res://graphics/forward.tres"))
	name_label.set("custom_colors/font_color",Color(0,0,0))
	self.add_child(name_label)
	
	print("TODO: Label class")
	var hp_label = Label.new()
	hp_label.name = "HpLabel"
	hp_label.rect_position = Vector2(-50, 0)
	hp_label.rect_size = Vector2(104,38)
	hp_label.margin_left = -50
	hp_label.margin_top = 0
	hp_label.margin_bottom = 40
	hp_label.margin_right = 50
	hp_label.align = HALIGN_CENTER
	hp_label.valign = VALIGN_CENTER
	hp_label.set("custom_fonts/font", load("res://graphics/forward.tres"))
	hp_label.set("custom_colors/font_color",Color(1,0,0))
	self.add_child(hp_label)
	
	var active_abilities = Node2D.new()
	active_abilities.name = "ActiveAbilities"
	self.add_child(active_abilities)
	
	self.add_child(DamageTypes.new())
	
	var portrait = Sprite.new()
	portrait.name = "Portrait"
	portrait.hide()
	self.add_child(portrait)
	
	_update_hp_display()
	get_resistances()


# Not a good (or used) system. Remove, rework later.
func get_resistances() -> float:
	var resistances = []
	resistances.insert($DamageTypes.PHYSICAL, 0.2)
	resistances.insert($DamageTypes.WORD, 0.0)
	resistances.insert($DamageTypes.ELEMENTAL, 0.0)
	resistances.insert($DamageTypes.MITE, 0.0)

	return resistances

func get_power_levels() -> float:
	var power_levels = []
	power_levels.insert($DamageTypes.PHYSICAL, 1.3)
	power_levels.insert($DamageTypes.WORD, 1.0)
	power_levels.insert($DamageTypes.ELEMENTAL, 0.8)
	power_levels.insert($DamageTypes.MITE, 1.0)

	return power_levels

# Not in use, remove
func get_battlecry() -> String:
	return battlecry_list[randi() % battlecry_list.size()]

# Better system: Listen for slot selected.
# If has ai, use it 
# But be wary of other things about to trigger, so that we don't trigger both player and ai
func execute_ai() -> void:
	$AI.use()

func _update_hp_display() -> void:
	$HpLabel.text = str(_current_hp) + "/" + str(_max_hp)

func take_damage(amount : float) -> float:
	_current_hp -= amount
	_update_hp_display()
	return _current_hp

# There is a better way to do this. Not sure how.
# Note the disastrous call in gui for this
func get_active_abilities():
	# Todo: as onready
	return get_node("ActiveAbilities").get_children()
	
func get_unit_name():
	return $NameLabel.text
	
func set_unit_name(new_name):
	$NameLabel.text = new_name
	name = new_name
	
func get_portrait_texture():
	return $Portrait.texture
