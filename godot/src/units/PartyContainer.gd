extends Node

class_name PartyContainer

var ordered_units

func _ready() -> void:
	self.name = "PartyContainer"

func export_units() -> Unit:
	ordered_units = get_children()
	for child in ordered_units:
		child.connect("return_to_container", self, "receive_unit")
		child.show()
		remove_child(child)

	return ordered_units

func receive_unit(unit) -> void:
	add_child(unit)
	unit.hide()

# Not sure where this is used?
func sort() -> void:
	print("Sort triggered")
	for unit in get_children():
		remove_child(unit)
	if (ordered_units.size() < 1):
		print("ERROR: Sort() in unitcontainer had no sorting array")
	for unit in ordered_units:
		add_child(unit)
