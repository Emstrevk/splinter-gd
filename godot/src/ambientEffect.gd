extends Node

onready var tween = get_node("Tween")
onready var sprite = get_node("Sprite")

var _origin
var _target
	
func _move(target: Vector2, origin : Vector2):
	tween.connect("tween_completed", self, "back_to_origin", [])
	_origin = origin
	_target = target
	
	# Inverse proportion to distance
	var speed_modifier = 4 * (origin.distance_to(target) / 2800) # Normal is whole length of base origin to reset x
	
	if (sprite.get_parent().position != target):
		tween.interpolate_property(sprite.get_parent(), "position", sprite.get_parent().position,
				target, speed_modifier * rand_range(4, 10), Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
		tween.start()

func back_to_origin(obj, key):
	self.position = Vector2(2200, _origin.y)
	_move(_target, Vector2(2200, _origin.y))
