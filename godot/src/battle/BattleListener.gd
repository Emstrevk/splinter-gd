extends Node2D

class_name BattleListener

signal turn_ended
signal end_defeat
signal end_victory

func _ready():
	
	self.name = "BattleListener"
	
	safe_connect("turn_ended", "battle_turn_ended")
	safe_connect("end_defeat", "battle_ended_defeat")
	safe_connect("end_victory", "battle_ended_victory")
	
	add_to_group("battle_listeners")
	
func safe_connect(listener_signal, parent_function):
	if (get_parent().has_method(parent_function)):
		connect(listener_signal, get_parent(), parent_function)	

# TYPING BROKEN
func turn_ended(battle_service):
	emit_signal("turn_ended", battle_service)
	
func end_defeat(battle_service):
	emit_signal("end_defeat", battle_service)
	
func end_victory(battle_service):
	emit_signal("end_victory", battle_service)
