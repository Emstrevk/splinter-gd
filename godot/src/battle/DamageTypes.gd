extends Node

# All neat for now, but perhaps not needded long term if we rework
# I also feel like we should use the new() keyword with these instead of adding them as children
class_name DamageTypes

var PHYSICAL = 0
var WORD = 1
var ELEMENTAL = 2
var MITE = 3

func _ready() -> void:
	self.name = "DamageTypes"
