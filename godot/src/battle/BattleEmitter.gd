extends Node2D

class_name BattleEmitter

var listener = "battle_listeners"

func _ready() -> void:
	self.name = "BattleEmitter"

func call(method_name):
	get_tree().call_group(listener, method_name, get_parent())

func turn_ended():
	call("turn_ended")
	
func end_victory():
	call("end_victory")
	
func end_defeat():
	call("end_defeat")
	
	
