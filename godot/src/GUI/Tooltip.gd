extends Control

# Should implement listeners to handle it's own opening and closing 
class_name Tooltip

func _ready():
	$Canvas.hide()

func display(title, message):
	$"Canvas/InfoText".text = message
	$"Canvas/Title".text = title
	$Canvas.show()

func close():
	$Canvas.hide()
