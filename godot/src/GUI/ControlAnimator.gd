extends Control


# This and other animators could use standardizing
# Only difference between this and other is name of properties; maybe they can be exported?
# Is there perhaps a way to autoload based on parent properties? Has_method?
# Goal is ONE ANIMATION CLASS containing all the needed standard animations 

var movement_time = 0.2

onready var tween_node = get_node("Tween")

var in_position = Vector2(0,0)
var hidden = Vector2(0,1000)

func _ready():
	
	var tween = Tween.new()
	tween.name = "Tween"
	self.add_child(tween)
	
	print("Warning: disabled deafult hidden position for control animator parent")
	#get_parent().rect_position = hidden

func send_in():
	# Scale doesn't seem to do anything
	tween_node.interpolate_property(get_parent(), "rect_position", hidden, in_position, movement_time, Tween.TRANS_SINE, Tween.EASE_OUT, 0)
	tween_node.interpolate_property(get_parent(), "rect_scale", 0.0, 1.0, movement_time, Tween.TRANS_SINE, Tween.EASE_OUT, 0)
	tween_node.start()

func send_out():
	tween_node.interpolate_property(get_parent(), "rect_position", get_parent().rect_position, hidden, movement_time, Tween.TRANS_SINE, Tween.EASE_OUT, 0)
	tween_node.interpolate_property(get_parent(), "rect_scale", 1.0, 0.0, movement_time, Tween.TRANS_SINE, Tween.EASE_OUT, 0)
	tween_node.start()
