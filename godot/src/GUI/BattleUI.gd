extends Control

class_name BattleUI

# Todo: dollar annotation
onready var player_info = get_node("PlayerInfo")
onready var target_info = get_node("TargetInfo")

var _hover_read_enabled = false

func _ready() -> void:
	close()

func target_info_on_hover(on):
	_hover_read_enabled = on

func read_unit(unit : Unit) -> void:
	if (unit != null):
		player_info.read_unit(unit)
		player_info.show()

func read_target_unit(unit : Unit) -> void:
	if (unit != null):
		target_info.read_unit(unit)
		target_info.show()

func slot_hovered(slot : Slot) -> void:
	if (_hover_read_enabled and slot.get_unit() != null):
		read_target_unit(slot.get_unit())

func slot_unhovered(slot : Slot) -> void:
	if (_hover_read_enabled):
		target_info.hide()

func close() -> void:
	player_info.hide()
	target_info.hide()

func battle_turn_ended(battle_service : BattleService) -> void:
	close()
