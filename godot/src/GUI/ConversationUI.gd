extends Control

func _ready():
	$RightBox.hide()
	$LeftBox.hide()
	$RightBoxBg.hide()
	$LeftBoxBg.hide()
	$SpeakerLeft.hide()
	$SpeakerRight.hide()
	yield(get_tree().create_timer(1), "timeout")
	
	
	### TODO: TEsting code
	#$TestConversation.get_child(0).select()
	
	
	# POrt this handling into own objects
	start()
	
func start():
	
	print(Vector2(-1000, $SpeakerLeft.rect_position.y))
	
	$SpeakerLeft.get_node("ControlAnimator").hidden = Vector2(-1000, $SpeakerLeft.rect_position.y)
	$SpeakerRight.get_node("ControlAnimator").hidden = Vector2(3000, $SpeakerLeft.rect_position.y)
	
	$SpeakerLeft.get_node("ControlAnimator").in_position = $SpeakerLeft.rect_position
	$SpeakerRight.get_node("ControlAnimator").in_position = $SpeakerRight.rect_position
	
	$SpeakerLeft.get_node("ControlAnimator").send_in()
	$SpeakerRight.get_node("ControlAnimator").send_in()

func read_node(conversation_node : ConversationNode):
	print("reading message: "  + conversation_node.message)
	
	$RightBox.hide()
	$LeftBox.hide()
	$RightBoxBg.hide()
	$LeftBoxBg.hide()
	
	# Might be prettier if portraits always stay but have bubbles
	if (conversation_node.from_player):
		$SpeakerLeft.show()
		$SpeakerLeft.texture = conversation_node.portrait
		$SlowReader.read_into($LeftBox, conversation_node.message)
		$LeftBox.show()
		$LeftBoxBg.show()
	else:
		$SpeakerRight.show()
		$SpeakerRight.texture = conversation_node.portrait
		$SlowReader.read_into($RightBox, conversation_node.message)
		$RightBox.show()
		$RightBoxBg.show()

func conversation_node_loaded(conversation_node : ConversationNode):
	$ConversationInput.load_conversation_node(conversation_node)
	
func conversation_node_selected(conversation_node : ConversationNode):
	print("DEBUG: Hiding buttons on selection, might be race condition...")
	$ConversationInput.hide_buttons()
	read_node(conversation_node)
	
func conversation_node_ended_conversation(conversation_node : ConversationNode):
	yield(get_tree().create_timer(1), "timeout")
	$SpeakerLeft.get_node("ControlAnimator").send_out()
	$SpeakerRight.get_node("ControlAnimator").send_out()
	$RightBoxBg.hide()
	$LeftBoxBg.hide()



