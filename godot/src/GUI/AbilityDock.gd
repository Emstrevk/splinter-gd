extends Control

class_name AbilityDock

var loaded_abilties = []
onready var ability_buttons = $Container.get_children()

# Don't reference gui. Emit signal that parent can use to hide tooltip. Or tooltip to hide tooltip.
onready var GUI = get_parent()

signal hide_tooltip

func _ready() -> void:
	
	add_child(AbilityListener.new())
	
	$Container.hide()
	for button in ability_buttons:
		button.connect("pressed", self, "button_clicked", [button])
		button.connect("mouse_entered", self, "button_mouse_entered", [button])
		button.connect("mouse_exited", GUI, "hide_tooltip", [])
	clear_abilities()

func load_abilities(ability_list) -> void:
	loaded_abilties = ability_list
	var index = 0
	for ability in ability_list:
		ability_buttons[index].show()
		ability_buttons[index].get_node("Icon").texture = ability.get_icon()
		index += 1

func clear_abilities() -> void:
	loaded_abilties = []
	for button in ability_buttons:
		button.hide()

func button_clicked(button) -> void:
	get_button_ability(button).select()

# Parent reference, intestead add ability hover listener in parent and trigger it here
func button_mouse_entered(button) -> void:
	get_parent().read_ability(get_button_ability(button))

func get_button_ability(button) -> Ability:
	return loaded_abilties[ability_buttons.find(button)]

func ability_selected(ability : Ability) -> void:
	$Container.hide()
	emit_signal("hide_tooltip")

func ability_failed(ability : Ability) -> void:
	$Container.show()
	emit_signal("hide_tooltip")

func ability_succeeded(ability : Ability) -> void:
	clear_abilities()
	$Container.hide()
	emit_signal("hide_tooltip")

func display():
	$Container.show()

func close():
	$Container.hide()
