extends Control

class_name InventorySlot

func _ready() -> void:
	self.add_child(MouseInput.new())


func input_enabled(boolean):
	if (boolean):
		$MouseInput.show()
	else:
		$MouseInput.hide()

func hovered():
	print("Inventory hovered")
	$HLBody.show()
	
func unhovered():
	$HLBody.hide()
