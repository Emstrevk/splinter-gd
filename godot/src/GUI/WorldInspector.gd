extends Control

func _ready() -> void:
	hide_buttons()
	for button in $ActionButtons.get_children():
		button.connect("pressed", $ControlAnimator, "send_out")
	disappear()

func inspect(world_object : WorldObject) -> void:
	print("Inspecting object")
	$ControlAnimator.send_in()
	hide_buttons()
	load_actions(world_object.get_actions())
	$SlowReader.read_into($TextBox, world_object.get_description())

func disappear() -> void:
	$ControlAnimator.send_out()
	# Todo: Listen to tween and call hide when off-screen

func hide_buttons() -> void:
	for button in $ActionButtons.get_children():
		button.hide()

func bounce() -> void:
	$ControlAnimator.bounce(get_node("TextureRect"), 0.2,0.2)

func load_actions(actions) -> void:
	var index = 0
	hide_buttons()

	for action in actions:
		$ActionButtons.get_child(index).show()
		$ActionButtons.get_child(index).set_action(action)
		index += 1

func world_object_clicked(wo: WorldObject):
	inspect(wo)
	
func world_slot_clicked(sl : Slot):
	disappear()
	
func zone_loaded(zone):
	if (zone.get_introduction_object() != null):
		inspect(zone.get_introduction_object())
	else:
		print("Error: Zone has no intro")
