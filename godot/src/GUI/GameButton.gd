extends TextureButton

var last_request

# Is this used consistently all over? 
func set_action(request):
	if (last_request != null):
		disconnect("pressed", last_request, "resolve")
	get_node("Label").text = request.title
	connect("pressed", request, "resolve")
	last_request = request
