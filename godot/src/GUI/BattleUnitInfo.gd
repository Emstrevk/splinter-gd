extends Node

# Rework if damage types go away
class_name BattleUnitInfo

func _ready() -> void:
	self.add_child(DamageTypes.new())

func read_unit(unit : Unit) -> void:
	$NameText.text = unit.get_unit_name()
	$HpText.text = "HP: " + str(unit._current_hp) + "/" + str(unit._max_hp)
	$Portrait.texture = unit.get_portrait_texture()
	var powers = unit.get_power_levels()
	$StatText.text = "Physical Power: " + str(powers[$DamageTypes.PHYSICAL])
