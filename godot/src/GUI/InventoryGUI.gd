extends Control

onready var item_list = $Panel/ItemList

onready var slot_reference = preload("res://scenes/GUI/InventorySlot.tscn")

var y_offset = 250
var x_offset = 230

func _ready():
	# This goes into grid objects
	for y in range(0,4):
		for i in range(0,7):
			var newSlot = slot_reference.instance()
			add_child(newSlot)
			newSlot.input_enabled(true)
			newSlot.rect_position = Vector2(rect_position.x + x_offset + i * 180,
					rect_position.y + y_offset + y * 180)
