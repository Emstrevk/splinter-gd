extends Node2D

# Kinda feel like this should be nested under the richtext itself and call parent
# Of course that would in turn require the label owners to have a reference to this
# (or an emitter function)
class_name SlowReader

var read_target
var full_message_length
var index

var interval : float = 0.03
var interval_counter : float = 0.0

var reading

# TOdo: Animate last letter / use some kind of cursor

func read_into(label : RichTextLabel, message : String) -> void:
	if (message != null):
		index = 0
		label.text = message
		label.set_visible_characters(index)
		read_target = label
		full_message_length = message.length()
		reading = true
		interval_counter = 0.0

func _process(delta) -> void:
	if (reading):
		if (index < full_message_length):
			interval_counter += delta
			if (interval_counter >= interval):
				index += 1
				read_target.set_visible_characters(index)
				interval_counter = 0.0
