extends Control


# In general none of these functions are "GUI" as in gui masterclass
# Abilitydock can contain it's own
# Tooltip can also contain it's own 
# Try to strip this even more.
# Abilitydock can close tooltip by making whatever call needs it register in toolt listener as closing time
class_name GUI

func _ready() -> void:
	self.add_child(Services.new())
	$AbilityDock.connect("hide_tooltip", $Tooltip, "close")

func read_ability(ability : Ability) -> void:
	$Tooltip.display(ability.get_name(), ability.get_tooltip())

func _load_abilities(ability_list) -> void:
	$AbilityDock.load_abilities(ability_list)
	$AbilityDock.display()

# TODO: Battle service signals
	
	
# Note: This is where the ai shit gets cemented
func slot_selected(slot):
	$BattleUI.target_info_on_hover(false)
	if (slot.is_ai_active()):
		$Tooltip.close()
		$BattleUI.read_target_unit(slot.get_unit())
	else:
		# Very bad call. But please avoid making a slot.get_ab instead, it won't be much better
		_load_abilities(slot.get_unit().get_active_abilities())
		$BattleUI.read_unit(slot.get_unit())
		$BattleUI.target_info_on_hover(true)

func battle_ended_defeat(battle_service : BattleService) -> void:
	$Tooltip.display("Losss", "u suck")






