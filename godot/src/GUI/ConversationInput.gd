extends Node2D

var _buttons_in_use = 0

func _ready():
	hide_buttons()
		
func hide_buttons() -> void:
	_buttons_in_use = 0
	for button in get_children():
		button.hide()

func load_conversation_node(node : ConversationNode) -> void:
	
	var new_request = Request.new()
	get_parent().add_child(new_request)
	new_request.connect("resolved", node, "select")
	
	new_request.title = node.title
	
	get_child(_buttons_in_use).show()
	
	get_child(_buttons_in_use).set_action(new_request)

	_buttons_in_use += 1