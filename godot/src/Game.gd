extends Node2D

class_name Game

var encounter_world_object

onready var switch_timer = Timer.new()

func _ready():
	
	self.add_child(Services.new())
	self.add_child(EncounterListener.new())
	self.add_child(BattleListener.new())
	
	switch_timer.set_wait_time(1)
	switch_timer.set_one_shot(true)
	self.add_child(switch_timer)
	$Services.BATTLE.hide()
	
# Not sure about end_battle, could listen to battle itself or just encounter
# POint is we probably want a "post-battle" screen at some point, which will take most data from encounter
# IMPORTANT: Battle must be able to hide self, encounter object should destroy self 
func encounter_triggered(encounter : Encounter) -> void:
	switch_timer.start()
	yield(switch_timer, "timeout")
	
	# Not sure how to go about encounter transfer; isn't it better if we keep zone unhidden?
	# Moving around is a bit wonky but non-hide might hit performance
	$Services.ZONE.hide()
	$Services.BATTLE.show()
	$Services.BATTLE.begin_battle(encounter)
	
# From listener
func battle_ended_victory(battle_service : BattleService) -> void:
	switch_timer.start()
	yield(switch_timer, "timeout")
	$Services.BATTLE.hide()
	$Services.ZONE.show()
	
	# Now back in "world"
	#asdasd # Current bug: Extract returns null if you kill 
	# everythin in the same turn
	var defeated_encounter : Encounter = battle_service.extract_encounter()
	defeated_encounter.return_to_world()
	defeated_encounter.defeated()
