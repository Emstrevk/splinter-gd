extends Node


class_name SlotAnimator

# See controlanimator class; should be uniform
# Many interpolations could be ported since they share a lot of args 
var tween_node

var movement_time = 1

func _ready() -> void:
	
	self.name = "SlotAnimator"
	
	var tween_node = Tween.new()
	tween_node.name = "Tween"
	self.add_child(tween_node)

func bounce(node, intensity, bounce_time):

	var base = 1
	var upper_base = base + intensity
	var lower_base = base - intensity
	var top_most = Vector2(node.scale.x * upper_base, node.scale.y * upper_base)
	var bottom_most = Vector2(node.scale.x * lower_base, node.scale.y * lower_base)
	tween_node.interpolate_property(node, "scale", node.scale, top_most, bounce_time, Tween.TRANS_SINE, Tween.EASE_OUT, 0)
	tween_node.interpolate_property(node, "scale", top_most, bottom_most, bounce_time, Tween.TRANS_SINE, Tween.EASE_OUT, bounce_time)
	tween_node.interpolate_property(node, "scale", bottom_most, node.scale, bounce_time, Tween.TRANS_SINE, Tween.EASE_OUT, bounce_time * 2)
	tween_node.start()

func strike(node, start_position, strike_position):
	var strike_time = 0.002
	var recover_time = 0.2

	tween_node.interpolate_property(node, "position", start_position, strike_position, strike_time, Tween.TRANS_SINE, Tween.EASE_OUT, 0)
	tween_node.interpolate_property(node, "position", strike_position, start_position, recover_time, Tween.TRANS_LINEAR, Tween.EASE_OUT, strike_time)
	tween_node.start()

func fade_sprite(sprite : Sprite, time, delay):
	tween_node.interpolate_property(sprite, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), time, Tween.TRANS_LINEAR, Tween.EASE_IN, delay)

func fade(node, delay):
	tween_node.interpolate_property(node.get_node("Sprite"), "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 0.3, Tween.TRANS_LINEAR, Tween.EASE_IN, delay)

func fade_slow(node, delay, time):
	tween_node.interpolate_property(node.get_node("Sprite"), "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), time, Tween.TRANS_LINEAR, Tween.EASE_IN, delay)

func move_steady(node, target: Vector2):
	if (node.position != target):
		tween_node.interpolate_property(node, "position", node.position, target, movement_time, Tween.TRANS_EXPO, Tween.EASE_OUT, 0)
		tween_node.start()

func move(node, target: Vector2, distance : int):
	if (node.position != target):
		tween_node.interpolate_property(node, "position", node.position, target, movement_time * distance, Tween.TRANS_ELASTIC, Tween.EASE_OUT, 0)
		tween_node.start()

func execute_after_tween(node, function):
	tween_node.connect("tween_completed", node, function)
