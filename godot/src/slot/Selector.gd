extends Sprite

class_name TargetIndicator

var tween_scale = 1.1

var tween_node
onready var tween_values = [scale, Vector2(scale.x * tween_scale, scale.y * tween_scale)]

export var anim_time = 1.0

func _ready():
	tween_node = Tween.new()
	self.add_child(tween_node)
	tween_node.repeat = true
	
	# Change as needed
	self.texture = load("res://Slot_target_indicator.png")
	
	#yield(get_tree().create_timer(0.5), "timeout")
	_start_tween()

func _start_tween():
	# Back and forth scale swell effect
	tween_node.interpolate_property(self, "scale", tween_values[0], tween_values[1], anim_time, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween_node.interpolate_property(self, "scale", tween_values[1], tween_values[0], anim_time, Tween.TRANS_LINEAR, Tween.EASE_OUT, anim_time)
	tween_node.interpolate_property(self, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0.5), anim_time, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween_node.interpolate_property(self, "modulate", Color(1, 1, 1, 0.5), Color(1, 1, 1, 1), anim_time, Tween.TRANS_QUAD, Tween.EASE_IN, anim_time)
	tween_node.start()
