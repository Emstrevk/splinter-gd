extends Node2D

class_name SlotRack

# Master class for a collection of slots

var _max_iterations_before_error = 10
var _current_iterations = 0

var slots = []
var iteration_index = 0

export var invert_order = false

func _add_slot() -> Slot:
	var slot = Slot.new()
	return slot

func _ready():
	
	_add_slot().position = Vector2(0,0)
	_add_slot().position = Vector2(100,100)
	_add_slot().position = Vector2(200,200)
	_add_slot().position = Vector2(300,300)
	_add_slot().position = Vector2(400,400)
	
	add_child(BattleListener.new())

	# Register slot children as layed out in editor
	var children = get_children()
	for child in children:
		if child is Slot:
			if (invert_order):
				slots.push_front(child as Slot)
			else:
				slots.push_back(child as Slot) # For some reason the bottom-most child is the first, so invert
	
# Purely cosmetic, a bit of legacy
func set_enemy_coloring():
	for slot in slots:
		slot.set_ai_active(true)
		slot.get_body().enemy_color(true)
		

func receive_units(units : Array) -> void:
	var index = 0
	for untyped_unit in units:
		var unit : Unit = untyped_unit
		slots[index].add_unit(unit)
		index += 1
		
func battle_ended_victory(battle_service) -> void:
	evacuate_units()
		
func evacuate_units() -> void:
	for untyped_slot in slots:
		var slot : Slot = untyped_slot
		if (slot.unit != null):
			slot.evacuate_unit()
		
func count_units() -> int:
	var count = 0
	for untyped_slot in slots:
		var slot : Slot = untyped_slot
		if (slot.unit != null):
			count += 1
	return count
	
# Increases index on call, returns null if no slot at index
func iterate() -> Slot:
	
	# TOdo: Don't call private method
	for slot in slots:
		slot._update_position()
		
	var get_slot = slots[iteration_index] as Slot 
	if (iteration_index < len(slots) - 1):
		iteration_index += 1
	else:
		iteration_index = 0
		
	if (get_slot.get_unit() != null):
		return get_slot
	else:
		return null
	
# Not sure if this is even used anywhere (search comes back blank)
func get_at(index : int) -> Slot:
	# Basic way to clean dead slots from the battlefield AFter a turn is ended
	# ... update_position, that is
	for slot in slots:
		#if (slot.unit == null):
		slot._update_position()
		
	if (slots[index] != null):
		if ((slots[index] as Slot).get_unit() != null):
			return slots[index] as Slot 
		else:
			return null
	else:
		print("ERROR: INdex out of bounds for slotRack.get_at()")
		return null
	
		
# Note: Only allows triggering if tank doesn't exist
func create_tank() -> Slot:
	if (get_tank() == null):
		for slot in slots:
			if (slot as Slot).unit != null:
				(slot as Slot).become_tank()
				return slot as Slot
		print ("ERROR: Found no viable new tank candidate")
		return null
	else:
		print("WARN: Tried to create tank while one existed")
		return null
			
func get_tank() -> Slot:
	for untyped_slot in slots:
		var slot : Slot = untyped_slot
		if (slot.unit != null):
			if (slot.slot_status().is_tank(slot)):
				return slot
	# No longer an error since this is a check too, but note error handling for target fetching
	#print("ERROR: No tank found, this should never happen!")
	return null
	
# Can be removed once we add group handling for exposed slots
# Notable however is difference between player_exposed and enemy_exposed
# Thus groups should be 
func get_all_exposed() -> Array:
	var all_exposed = []
	for slot in slots:
		if ((slot as Slot).slot_status().is_exposed(slot)):
			all_exposed.append(slot as Slot)
	return all_exposed
