extends Node

# Class for managing slot status (wraps around the group enums)
# TODO: "subject" should not be necessary; refactor to call
# get_parent as any other responsibilities should be handled
# elsewhere
class_name SlotStatus

func _ready() -> void:
	var status_groups = SlotStatusGroups.new()
	status_groups.name = "SlotStatusGroups"
	add_child(status_groups)

func _reclassify(subject, new_status: String) -> void:
	clear(subject)
	subject.add_to_group(new_status)

func clear(subject) -> void:
	subject.remove_from_group($SlotStatusGroups.EXPOSED)
	subject.remove_from_group($SlotStatusGroups.NORMAL)
	subject.remove_from_group($SlotStatusGroups.TANK)

func to_tank(subject) -> void: _reclassify(subject, $SlotStatusGroups.TANK)
func to_normal(subject) -> void: _reclassify(subject, $SlotStatusGroups.NORMAL)
func to_exposed(subject) -> void: _reclassify(subject, $SlotStatusGroups.EXPOSED)
	
func is_tank(subject) -> bool: return subject.is_in_group($SlotStatusGroups.TANK)
func is_normal(subject) -> bool: return subject.is_in_group($SlotStatusGroups.NORMAL)
func is_exposed(subject) -> bool: return subject.is_in_group($SlotStatusGroups.EXPOSED)
