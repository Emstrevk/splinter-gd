extends Node2D

class_name SlotEmitter

var listener = "slot_listeners"



func _ready() -> void:
	self.name = "SlotEmitter"

func selected():
	get_tree().call_group(listener, "selected", get_parent())

func hovered():
	get_tree().call_group(listener, "hovered", get_parent())
	
func unhovered():
	get_tree().call_group(listener, "unhovered", get_parent())
	
func clicked():
	get_tree().call_group(listener, "clicked", get_parent())
	
func moved():
	get_tree().call_group(listener, "moved", get_parent())
