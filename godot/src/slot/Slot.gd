extends Node2D

class_name Slot

onready var _body = $SlotGraphics

var unit : Unit
var _ai_active = false

signal slot_clicked

func _ready():
	
	add_child(Services.new())
	
	add_child(SlotGraphics.new())
	add_child(SlotMover.new())
	add_child(Shaker.new())
	add_child(SlotEmitter.new())
	add_child(SplashReceiver.new())
	add_child(SlotListener.new())
	
	# Mouseinput broken here because I deleted it 
	# TODO: the "collision from sprite" is a good start
	# but is there a way to do it dynamically?
	add_child(MouseInput.new())

	var slot_status = SlotStatus.new()
	slot_status.name = "SlotStatus"
	add_child(slot_status)
	
	slot_status().to_normal(self)
	
	_body.update_decal(self)
	
func slot_status() -> SlotStatus: return $SlotStatus as SlotStatus
	
# TODO: Not pretty, but can't think of how to do it 
# First of all though getter/setter is pointless, use public var instead
# Check can also be moved into some local trigger_ai() function to avoid exposing too much
func set_ai_active(boolean):
	_ai_active = boolean
	
func is_ai_active():
	return _ai_active
	
# Pointless getter
func get_unit() -> Unit:
	return unit

func clicked():
	$SlotEmitter.clicked()

func select():
	$SlotMover.bounce(0.1, 0.15)
	_body.selector(true)
	$SlotEmitter.selected()
	
func animate_strike():
	$SlotMover.strike()
		
		
# Investigate setter pattern for gdscript
# Basically we could perform all these operations when a new value is set to unit
func add_unit(new_unit):
	if (unit != null):
		print("ERROR: Tried to add unit to slot with existing unit")
		get_tree().quit()

	unit = new_unit
	add_child(unit)
	unit.show()
	_body.dead(false)
	_body.unit_decal(true)
	
	
# Technically we could refer to units directly
# But given the current ability system etc. this is as sound as it gets for now
func damage_unit(amount : float):
	if (unit != null):
		var hp_left = unit.take_damage(amount)
		if (hp_left > 0):
			$Shaker.shake(0.6, 10)
		else:
			_kill_unit()
		
# Get_parent call needs to go, see detauls on request_new_tank in slotrack
func _kill_unit():
	$Shaker.shake(0.6, 15)
	
	unit._current_hp = 0
	_body.enemy_color(false)

	evacuate_unit()
	# Careful with tanks
	slot_status().clear(self)
	_body.update_decal(self)
	_body.dead(true)
	_body.highlight(false)
	_body.unit_decal(false)
	
# Could technically be handled by unit itslef
# If added to group player_units we can make them all hide and return to container
# Signal itself is a bit... I mean if we ever make a unitemitter that would be better
# A unitemitter could also handle death better
func evacuate_unit():
	unit.hide()
	remove_child(unit)
	unit.emit_signal("return_to_container", unit)
	unit = null
	slot_status().clear(self)
		
func _update_position():
	$SlotMover.move_to_matching(self)

func de_select():
	_body.selector(false)

# Note: Listener functions should ideally be placed lowest with a comment header
func hovered():
	if (get_unit() != null):
		_body.highlight(true)
	$SlotEmitter.hovered()

func unhovered():
	_body.highlight(false)
	$SlotEmitter.unhovered()

# Get parent should go.
# I feel expose itself should be type-safe; you cannot expose tanks
# So make a can_expose and check with that before calling this (also build it in)
# That way we can fail abilities trying to expose tank
func expose():
	var was_tank = slot_status().is_tank(self)
	$SlotEmitter.moved()
	slot_status().to_exposed(self)
	_body.update_decal(self)
	_update_position()
	
func become_tank():
	$SlotEmitter.moved()
	slot_status().to_tank(self)
	_body.update_decal(self)
	_update_position()
	
# Used by enemy coloring thingie. 
# When we start using groups, we can easily make a set_enemey_rack for the rack
# That one will call set_enemy in slots, which changes color AND handles group addition 
# Or Idunno. 
func get_body() -> SlotGraphics:
	return _body
	
# Ensure only one slot is ever selected
func slot_selected(slot):
	if (self != slot):
		de_select()
	
	
	
