extends Node2D

class_name SlotMover

onready var slot_animator = $"SlotAnimator"

var default_position: Vector2
var exposed_position: Vector2
var tank_position: Vector2

var _position_on_ready: Vector2
var position_distance = 65
var current_position_code

# TODO: This is basically legacy, everything should be ported to slot_animator eventually
# The logic here with positions should be in some other class

func _ready():
	
	self.add_child(SlotAnimator.new())
	
	var status_groups = SlotStatusGroups.new()
	status_groups.name = "SlotStatusGroups"
	add_child(status_groups)

	_setup_positions()
	slot_animator.movement_time = 0.4
	_position_on_ready = get_parent().global_position



func _get_screen_center() -> Vector2:
	return get_viewport_rect().size * 0.5

# Ready an index to
func _setup_positions():
	var screen_position = get_parent().global_position
	default_position = get_parent().get_position()
	if (screen_position <= _get_screen_center()):
		exposed_position = Vector2(default_position.x + position_distance, default_position.y - position_distance)
		tank_position = Vector2(default_position.x + position_distance * 2, default_position.y - position_distance * 2)
	else:
		exposed_position = Vector2(default_position.x - position_distance, default_position.y + position_distance)
		tank_position = Vector2(default_position.x - position_distance * 2, default_position.y + position_distance * 2)



func bounce(intensity, bounce_time):
	slot_animator.bounce(get_parent(), intensity, bounce_time)

func strike():
	var strike_position
	var strike_distance = 15

	var start_position = get_parent().get_position()
	if (_position_on_ready <= _get_screen_center()):
		strike_position = Vector2(start_position.x + strike_distance, start_position.y - strike_distance)
	else:
		strike_position = Vector2(start_position.x - strike_distance, start_position.y + strike_distance)

	slot_animator.strike(get_parent(), start_position, strike_position)

func fade(delay):
	slot_animator.fade(get_parent(), delay)

func _move(target: Vector2, distance : int):
	slot_animator.move(get_parent(), target, distance)
	#slot_animator.move_steady(get_parent(), target)

# TODO: Typing broken
func move_to_matching(subject):
	
	# TODO: Not neat
	if (current_position_code == null):
		current_position_code = 0
	

	# "Position code" is a rewrite from when 
	# status codes were enums. It is only used
	# to calculate distance and should probably 
	# be baked into the animation call itself
	# rather than set here.
	var to_position = default_position
	var old_position_code = current_position_code

	# TODO: Death is not handled properly
	# e.g. a dead slot may be in either position
	# But will count as "0" due to defaults and status.clear()
	if (subject.slot_status().is_tank(subject)):
		to_position = tank_position
		current_position_code = 2
	if (subject.slot_status().is_normal(subject)):
		to_position = default_position
		current_position_code = 0
	if (subject.slot_status().is_exposed(subject)):
		to_position = exposed_position
		current_position_code = 1
	
	var distance = abs(old_position_code - current_position_code)
	_move(to_position, distance)
