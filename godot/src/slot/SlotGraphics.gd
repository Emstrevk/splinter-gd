extends Node2D

class_name SlotGraphics

var tank_decal_texture = preload("res://tank_decal.png")
var exposed_decal_texture = preload("res://exposed_decal.png")

func _add_with_name(node, name) -> Node2D:
	self.add_child(node)
	node.name = name
	return node

func _ready():
	
	self.name = "SlotGraphics"
	
	_add_with_name(Sprite.new(), "Sprite").texture = load("res://graphics/slot/IsometricSlotBody.png")
	_add_with_name(Sprite.new(), "DeadSprite").texture = load("res://graphics/slot/WorldObjectDark.png")
	_add_with_name(Sprite.new(), "HighLight") # TODO
	_add_with_name(Sprite.new(), "EnemySlotMask").texture = load("res://graphics/slot/enemyObjectMask.png")
	_add_with_name(TargetIndicator.new(), "Selector").texture = load("res://graphics/slot/objectSelector.png")
	_add_with_name(TargetIndicator.new(), "TargetIndicator").texture = load("res://graphics/slot/objectSelector.png")
	var decal = _add_with_name(Sprite.new(), "StatusDecal")
	decal.position = Vector2(-65, 65)
	
	add_child(AbilityListener.new())
	
	dead(true)
	$Selector.hide()
	$TargetIndicator.hide()
	$HighLight.hide()
	$EnemySlotMask.hide()
	$StatusDecal.hide()

func unit_decal(on: bool) -> void:
	if on:
		$StatusDecal.show()
	else:
		$StatusDecal.hide()

func selector(on : bool) -> void:
	if on:
		$Selector.show()
	else:
		$Selector.hide()

func enemy_color(on : bool) -> void:
	if on:
		$EnemySlotMask.show()
	else:
		$EnemySlotMask.hide()

func dead(on : bool) -> void:
	if on:
		$DeadSprite.show()
		$Sprite.hide()
	else:
		$DeadSprite.hide()
		$Sprite.show()

# TODO: Wasn't allowed to static type
func update_decal(slot):
	$StatusDecal.show()
	if (slot.slot_status().is_exposed(slot)):
		$StatusDecal.texture = exposed_decal_texture
	elif (slot.slot_status().is_tank(slot)):
		$StatusDecal.texture = tank_decal_texture
	else:
		$StatusDecal.show()

func highlight(on : bool) -> void:
	if on:
		$HighLight.show()
	else:
		$HighLight.hide()

func targeted(on : bool) -> void:
	if on:
		$TargetIndicator.show()
	else:
		$TargetIndicator.hide()

# A bit wonky with the get_parent
func ability_new_targets(ability):
	if (ability.get_targets().has(get_parent())):
		targeted(true)
	else:
		targeted(false)

# TODO: Might be we want a "finished" emission for general cases
func ability_succeeded(ability):
	targeted(false)

func ability_failed(ability):
	targeted(false)
