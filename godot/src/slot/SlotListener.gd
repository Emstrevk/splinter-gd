extends Node2D

class_name SlotListener

signal selected
signal hovered
signal unhovered
signal clicked
signal moved

func _ready():
	safe_connect("selected", "slot_selected")
	safe_connect("hovered", "slot_hovered")
	safe_connect("unhovered", "slot_unhovered")
	safe_connect("clicked", "slot_clicked")
	safe_connect("moved", "slot_moved")

	add_to_group("slot_listeners")

func safe_connect(listener_signal, parent_function):
	if (get_parent().has_method(parent_function)):
		connect(listener_signal, get_parent(), parent_function)

func selected(slot):
	emit_signal("selected", slot)

func hovered(slot):
	emit_signal("hovered", slot)

func clicked(slot):
	emit_signal("clicked", slot)

func moved(slot):
	emit_signal("moved", slot)

func unhovered(slot):
	emit_signal("unhovered", slot)
