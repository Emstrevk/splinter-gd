extends Node

class_name SlotStatusGroups

const EXPOSED = "slot_exposed_group"
const NORMAL = "slot_normal_group"
const TANK = "slot_tank_group"

func get_all_exposed() -> Array: return get_tree().get_nodes_in_group(EXPOSED)
func get_all_tanks() -> Array: return get_tree().get_nodes_in_group(TANK)
func get_all_normal() -> Array: return get_tree().get_nodes_in_group(NORMAL)

