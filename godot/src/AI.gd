extends Node2D

# Contains lots of violations.
# Should use target fetchers like abilities instead of having its own logic for targets.
class_name AI

onready var _available_abilities = get_parent().get_active_abilities()

export var think_time = 0.3

onready var think_timer = Timer.new()

func _ready():
	
	self.add_child(Services.new())
	
	# TODO: Tree timer
	think_timer.set_wait_time(think_time)
	think_timer.set_one_shot(true)
	self.add_child(think_timer)

func use():

	# Target fetcher can do this
	# Problem: How to we get the player tank the easiest?
	var target = $Services.BATTLE.get_player_rack().get_tank()

	think_timer.start()
	yield(think_timer, "timeout")

	_available_abilities[0].target_manually(target)

	think_timer.start()
	yield(think_timer, "timeout")
	_available_abilities[0].execute(target)
