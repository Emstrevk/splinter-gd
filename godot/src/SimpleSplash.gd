extends Node2D

onready var death_timer = Timer.new()
	
# TOdo: Either hot-append a background sprite to this or rename the class ("abilitySplash")
func fire(preloaded_texture):
	$IconSprite.texture = preloaded_texture
	$SlotAnimator.bounce(self, 3, 0.05)
	$SlotAnimator.fade_sprite($Sprite, 0.3, 0.4)
	$SlotAnimator.fade_sprite($IconSprite, 0.3, 0.4)

	death_timer.set_wait_time(1)
	death_timer.set_one_shot(true)
	self.add_child(death_timer)
	death_timer.start()
	yield(death_timer, "timeout")

	get_parent().remove_child(self)
	queue_free()
