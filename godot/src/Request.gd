extends Node2D

class_name Request

# Usage: Connect a function to the signal and send it to a resolver. You can now queue functions. Nice.
signal resolved

var title

var one_shot = true

func resolve():
	emit_signal("resolved")
	if (one_shot):
		# Todo: Not sure why this would happen
		# Request is sort of deprecated either way
		if (get_parent() != null):
			get_parent().remove_child(self)
		else:
			print("ERROR: Request had no parent")
		queue_free()

func persist():
	one_shot = false
