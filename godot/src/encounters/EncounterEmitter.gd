extends Node2D

class_name EncounterEmitter

var _listener = "encounter_listeners"

func triggered() -> void:
	get_tree().call_group(_listener, "triggered", get_parent() as Encounter)
	
func defeated() -> void:
	print("EncounterEmitter: defeated! " + get_parent().name)
	get_tree().call_group(_listener, "defeated", get_parent() as Encounter)
