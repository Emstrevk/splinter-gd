extends Node2D

class_name EncounterListener

signal triggered
signal defeated

func _ready():
	self.name = "EncounterListener"

	safe_connect("triggered", "encounter_triggered")
	safe_connect("defeated", "encounter_defeated")
	
	add_to_group("encounter_listeners")

func safe_connect(listener_signal, parent_function):
	if (get_parent().has_method(parent_function)):
		connect(listener_signal, get_parent(), parent_function)
		
func triggered(encounter : Encounter):
	emit_signal("triggered", encounter)
	
func defeated(encounter : Encounter):
	emit_signal("defeated", encounter)
