extends Node

# Should hold other stuff like rewards and perhaps text data
# Note the factory methods; they can be removed
# HOWEVER it would be nice to learn how to handle additions before ready
class_name Encounter

# Precision signal for 1:1 telling of original
#	world object parent that battle resulted in defeat
signal return_to_world

var queued_units = []

func _ready():
	
	add_child(PartyContainer.new())
	add_child(EncounterEmitter.new())
	
	
	for unit in queued_units:
		$PartyContainer.receive_unit(unit)

func add_unit(unit : Unit):
	queued_units.append(unit)

func export_units():
	return $PartyContainer.export_units()

# Created for the battle->world transition which
#	relies on shifting parent objects
func return_to_world():
	emit_signal("return_to_world", self)
	
func defeated():
	$EncounterEmitter.defeated()
	
# Essentially a "leave world and enter battle" method
func trigger():
	$EncounterEmitter.triggered()
	get_parent().remove_child(self)
