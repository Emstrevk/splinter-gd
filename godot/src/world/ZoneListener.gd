extends Node2D

signal loaded

class_name ZoneListener

func _ready():
	safe_connect("loaded", "zone_loaded")
	
	add_to_group("zone_listeners")

func safe_connect(listener_signal, parent_function):
	if (get_parent().has_method(parent_function)):
		connect(listener_signal, get_parent(), parent_function)
		
func loaded(zone):
	emit_signal("loaded", zone)
