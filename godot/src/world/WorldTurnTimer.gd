extends Node2D

class_name WorldTurnTimer

var turns : int
var last_turn_set : int
var active : bool

signal turn_timer_ended

func _ready() -> void:
	self.name = "WorldTurnTimer"
	
	# TODO: Make own class since it's so configured
	var label = Label.new()
	label.name = "NumberLabel"
	label.margin_right = 17
	label.margin_bottom = 24
	label.rect_size = Vector2(17,24)
	label.rect_scale = Vector2(2,2)
	label.set("custom_fonts/font", load("res://graphics/forward.tres"))
	label.set("custom_colors/font_color",Color(1,0,0))
	self.add_child(label)
	
	self.add_child(WorldTurnListener.new())
	
	update_label()

func set_timer(turn_amount : int) -> void:
	turns = turn_amount
	last_turn_set = turns
	active = true
	update_label()

func update_label() -> void:
	if (turns != null):
		$NumberLabel.text = str(turns)
		$NumberLabel.show()
	else:
		$NumberLabel.hide()
	
func reset() -> void:
	turns = last_turn_set
	update_label()
	
func world_turn_taken() -> void:
	if (active):
		turns -= 1
		update_label()
		if (turns < 1):
			active = false
			print("Turn timer ended")
			emit_signal("turn_timer_ended")
		
