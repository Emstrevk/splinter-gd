extends Node2D

# More like "WorldSlotGrid"
# Might have some things in common with inventory or other grids
# But not sure if extending is really all that much cleaner
class_name SlotGrid

var WORLD_SLOT_DISTANCE = 125
var WORLD_PADDING_X = 150
var WORLD_PADDING_Y = 150

var GRID_HEIGHT = 6
var GRID_WIDTH = 13

var darken = 1.0

var world_slot_map = []

var highlighted_slots = []

func _new_slot(row: Node2D, name: String, position: Vector2) -> void:
	var slot = WorldSlot.new()
	slot.name = name
	slot.position = position
	row.add_child(slot)

func _ready():
	
	print("TODO: SlotGrid should not set its own position")
	# Also 
	self.position = Vector2(920, 200)
	
	self.name = "SlotGrid"
	
	var first_row = Node2D.new()
	self.add_child(first_row)
	_new_slot(first_row, "one", Vector2(0,0))
	_new_slot(first_row, "two", Vector2(110,110))
	_new_slot(first_row, "three", Vector2(220,220))
	_new_slot(first_row, "four", Vector2(330,330))
	
	var second_row = Node2D.new()
	self.add_child(second_row)
	_new_slot(first_row, "one", Vector2(-110,110))
	_new_slot(first_row, "two", Vector2(0,220))
	_new_slot(first_row, "three", Vector2(110,330))
	_new_slot(first_row, "four", Vector2(220,440))	
	
	var third_row = Node2D.new()
	self.add_child(third_row)
	_new_slot(third_row, "one", Vector2(-220,220))
	_new_slot(third_row, "two", Vector2(-110,330))
	_new_slot(third_row, "three", Vector2(0,440))
	_new_slot(third_row, "four", Vector2(110,550))		
	
	var fourth_row = Node2D.new()
	self.add_child(fourth_row)
	_new_slot(fourth_row, "one", Vector2(-330,330))
	_new_slot(fourth_row, "two", Vector2(-220,440))
	_new_slot(fourth_row, "three", Vector2(-110,550))
	_new_slot(fourth_row, "four", Vector2(0,660))			
	
	for row in get_children():
		for column in row.get_children():
			# Why no work??
			#if row is WorldSlot and column is WorldSlot:
			column.set_coordinates(Vector2(column.get_index(), row.get_index()))

func get_slot_at(coordinate_vector) -> WorldSlot:
	if (get_child(coordinate_vector.y) != null):
		return get_child(coordinate_vector.y).get_child(coordinate_vector.x) as WorldSlot
	return null
