extends Node2D

class_name MouseInput

signal clicked

# Added cause I don't trust the ready signal
signal more_than_ready

func set_collision_shape(shape):
	#$Area2D.get_child(0).clear_shapes()
	$Area2D.get_child(0).shape = shape

func _ready():
	self.name = "MouseInput"

		
	var area = Area2D.new()
	area.name = "Area2D"
	var collision_shape : CollisionShape2D = CollisionShape2D.new()
	add_child(area)
	area.add_child(collision_shape)

	# TODO: boundaries should be dynamic function
	collision_shape.set_position(Vector2(-7, -9))
	#var shape : RectangleShape2D = RectangleShape2D.new()
	var shape : CircleShape2D = CircleShape2D.new()
	#shape.extents = Vector2(60,60)
	shape.radius = 75
	set_collision_shape(shape)

	# These functions are expected to exist in the parent
	get_node("Area2D").connect("mouse_entered", get_parent(), "hovered")
	get_node("Area2D").connect("mouse_exited", get_parent(), "unhovered")
	get_node("Area2D").connect("input_event", self, "area_input_event")
	connect("clicked", get_parent(), "clicked")
	
	emit_signal("more_than_ready")
	
func area_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton \
		and event.button_index == BUTTON_LEFT \
		and event.pressed:
			emit_signal("clicked")
