extends Node

class_name Zone

# TODO: Tree timer 
var init_timer : Timer = Timer.new()

func _ready() -> void:
	
	var background = Sprite.new()
	background.name = "Background"
	background.texture = load("res://background.png")
	self.add_child(background)
	background.position = Vector2(920, 545)
	
	self.add_child(SlotGrid.new())
	self.add_child(WorldObjectContainer.new())
	self.add_child(WorldObjectListener.new())
	self.add_child(WorldSlotListener.new())
	self.add_child(WorldSlotListener.new())
	var intro_dock = Node2D.new()
	intro_dock.name = "IntroductionObjectDock"
	self.add_child(intro_dock)
	self.add_child(ZoneEmitter.new())
	self.add_child(EncounterListener.new())
	
	# TODO: This is a problem; can't have timer before
	# child init..?
	add_child(init_timer)
	init_timer.set_one_shot(true)
	init_timer.start()
	init_timer.set_wait_time(0.5)
	yield(init_timer, "timeout")

	# TOdo: Not the real place, should be called from some actual load function
	$ZoneEmitter.loaded()
	
	print("TODO: Zone has intro and object additions hardcoded")
	var intro = WorldObject.new()
	# Starting position determines placement
	# If left as 0,0 it will be put in a slot via listner
	# Yes, this needs to be changed.
	
	intro._starting_position = Vector2(-1,-1)
	intro.description = "This is the intro object"
	
	$IntroductionObjectDock.add_child(intro)
	
# Kinda should be in world_objects BUT that one has get_children base so kinda offputting
func world_slot_clicked(world_slot : WorldSlot) -> void:
	$WorldObjectContainer.sort()

func get_introduction_object():
	return $IntroductionObjectDock.get_child(0)
