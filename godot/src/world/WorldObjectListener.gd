extends Node2D

# TODO: Typing broken
class_name WorldObjectListener

signal world_object_clicked
signal world_object_hovered
signal world_object_began_moving
signal world_object_finished_moving
signal world_object_died
signal became_movable
signal unclicked
signal spawned

func _ready():
	
	self.name = "WorldObjectListener"
	
	# Implement methods in parent by need
	safe_connect("world_object_clicked", "world_object_clicked")
	safe_connect("world_object_hovered", "world_object_hovered")
	safe_connect("world_object_began_moving", "world_object_began_moving")
	safe_connect("world_object_finished_moving", "world_object_finished_moving")
	safe_connect("world_object_died", "world_object_died")
	safe_connect("became_movable", "world_object_became_movable")
	safe_connect("unclicked", "world_object_unclicked")
	safe_connect("spawned", "world_object_spawned")
	
	# One listener, one group
	add_to_group("world_object_listeners")
	
func safe_connect(listener_signal, parent_function):
	if (get_parent().has_method(parent_function)):
		connect(listener_signal, get_parent(), parent_function)
		
func unclicked(object):
	emit_signal("unclicked", object)
	
func clicked(object):
	emit_signal("world_object_clicked", object)

func hovered(object):
	emit_signal("world_object_hovered", object)
	
func began_moving(object):
	emit_signal("world_object_began_moving", object)
	
func finished_moving(object):
	emit_signal("world_object_finished_moving", object)
	
func died(object):
	emit_signal("world_object_died", object)
	
func became_movable(object):
	emit_signal("became_movable", object)
	
func spawned(object):
	emit_signal("spawned", object)
	

