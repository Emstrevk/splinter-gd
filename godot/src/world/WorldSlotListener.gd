extends Node2D

# TODO: Typing broken
class_name WorldSlotListener

signal clicked
signal hovered

func _ready():
	safe_connect("clicked", "world_slot_clicked")
	safe_connect("hovered", "world_slot_hovered")
	
	add_to_group("world_slot_listeners")
	
func safe_connect(listener_signal, parent_function):
	if (get_parent().has_method(parent_function)):
		connect(listener_signal, get_parent(), parent_function)

func clicked(slot ):
	emit_signal("clicked", slot)
	
func hovered(slot ):
	emit_signal("hovered", slot)
