extends Node2D

class_name ZoneEmitter

var listener = "zone_listeners"

func _ready() -> void:
	self.name = "ZoneEmitter"

func call(method_name):
	get_tree().call_group(listener, method_name, get_parent())

func loaded():
	call("loaded")
