extends Node2D

class_name WorldTurnListener

signal turn_taken

func _ready() -> void:
	safe_connect("turn_taken", "world_turn_taken")
	
	add_to_group("world_turn_listeners")

func safe_connect(listener_signal, parent_function):
	if (get_parent().has_method(parent_function)):
		connect(listener_signal, get_parent(), parent_function)

func turn_taken() -> void:
	emit_signal("turn_taken")
