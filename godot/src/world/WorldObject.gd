extends Node2D

# Haven't had a good look at all the functions yet...
class_name WorldObject

# Can be an allegiance object
var ALLEGIANCE_GOOD = 0
var ALLEGIANCE_EVIL = 1

export(Vector2) var _starting_position

var battle_power = 0

# Should be reworked; objects should be children of their slot
# Similar to battle slot/unit logic is a possible avenue
var current_slot

# Standin for more advanced combat behaviour
var allegiance = ALLEGIANCE_GOOD

var description = "WORLD OBJECT: No description available"

func icon(texture) -> void:
	$Icon.texture = texture

func _ready():
	
	var sprite = Sprite.new()
	sprite.name = "Sprite"
	sprite.texture = load("res://graphics/slot/WorldObject.png")
	self.add_child(sprite)
	# TODO: TEmp during transition
	self.move_child(sprite, 0)
	
	var icon = Sprite.new()
	icon.name = "Icon"
	self.add_child(icon)
	
	var mask = Sprite.new()
	mask.name = "EnemySlotMask"
	mask.texture = load("res://graphics/slot/enemy_slot_mastk.png")
	mask.hide()
	self.add_child(mask)
	
	var slot_anim = SlotAnimator.new()
	slot_anim.name = "SlotAnimator"
	self.add_child(slot_anim)
	
	var highlight = TargetIndicator.new()
	highlight.name = "TargetIndicator"
	highlight.texture = load("res://graphics/slot/WorldObjectIndicator.png")
	self.add_child(highlight)
	highlight.hide()
	
	var action_dock = Node2D.new()
	action_dock.name = "ActionDock"
	self.add_child(action_dock)
	
	var interaction_dock = Node2D.new()
	interaction_dock.name = "InteractionDock"
	self.add_child(interaction_dock)
	
	var wo_emitter = WorldObjectEmitter.new()
	self.add_child(wo_emitter)
	
	var wo_listener = WorldObjectListener.new()
	self.add_child(wo_listener)
	
	var ws_mover = WorldSlotMover.new()
	ws_mover.name = "WorldSlotMover"
	self.add_child(ws_mover)
	
	var cb_m = WorldObjectCombatModule.new()
	self.add_child(cb_m)
	
	var desc = Label.new()
	desc.name = "Description"
	desc.text = "No description found"
	self.add_child(desc)
	
	self.add_child(WorldObjectEncounterDock.new())

	# Actions need an object parent set up
	for action in $ActionDock.get_children():
		action.set_world_object_parent(self)

	# Interactions need an object parent set up
	for interaction in $InteractionDock.get_children():
		interaction.set_world_object_parent(self)

	# Hide from view until spawnning
	#self.position = Vector2(9999,9999)
	# Super-easy timer example
	# Wait a bit before calling spawn to make sure we don't piss in everything
	yield( get_tree().create_timer(0.5), "timeout" )
	$WorldObjectEmitter.spawned()

	var mousein = MouseInput.new()
	#mousein.set_position(self.global_position)
	self.add_child(mousein)
	#print("DEBUG: Connecting mouse ready signal")
	#yield(mousein, "tree_entered")
	yield( get_tree().create_timer(0.5), "timeout" )
	print("TODO: Working polygonal collision shape setter")
	#var new_shape = load("res://scenes/WorldObjectCollisionShape.tscn")
	#mousein.set_collision_shape(new_shape)
	#$MouseInput.set_collision_shape(.instance())
	#$MouseInput.position = self.global_position
	#self.move_child($MouseIn, self.get_child_count())
	#print_tree()

# Todo: circ ref needs to go
func move_to(world_slot):
	
	if (current_slot != null):
		current_slot.world_object_occupant = null
	$SlotAnimator.move_steady(self, world_slot.get_object_position())
	current_slot = world_slot
	current_slot.world_object_occupant = self
	$WorldObjectEmitter.began_moving()

# Starting position: WHere to place on zone ready
func get_starting_position():
	return _starting_position

func place(world_slot):
	if (current_slot != null):
		current_slot.world_object_occupant = null
	self.position = world_slot.get_object_position()
	current_slot = world_slot
	current_slot.world_object_occupant = self
	print("TODO: fix place() anim for animator/object")
	#alert()
	#print("DEBUG: CUrrent slot is: " + current_slot.name)

func alert():
	$SlotAnimator.bounce(self, 0.2,0.2)

func get_position():
	if (current_slot == null):
		return null
	return current_slot.coordinates

func is_close(object) -> bool:
	if (object.get_position() == null or get_position() == null):
		return false
	return object.get_position().distance_to(get_position()) < 2

func world_object_began_moving(object):
	if (object != self):
		if (is_close(object)):
			interact(object)

func clicked():
	print("DEBUG: Clicked a world object")
	$WorldObjectEmitter.clicked()

func unclicked():
	$WorldObjectEmitter.unclicked()

func hovered():
	$TargetIndicator.show()
	$WorldObjectEmitter.hovered()

func set_icon(texture):
	if ($Icon != null):
		$Icon.texture = texture

func interact(object):
	for interaction in $InteractionDock.get_children():
		interaction.trigger(object)

func fight(object : WorldObject):
	$ObjectCombatModule.fight(object)

func start_encounter():
	$WorldObjectEncounterDock.start_encounter()

func get_actions():
	return $ActionDock.get_children()

func unhovered():
	$TargetIndicator.hide()

func destroy():
	print("Killing object")
	$WorldObjectEmitter.died()
	$Icon.hide()
	$SlotAnimator.fade_slow(self, 0, 1.5)
	current_slot.world_object_occupant = null
	current_slot = null
	$WorldObjectEmitter.disable()

	# This queues a method call AFTER the next animation
	$SlotAnimator.execute_after_tween(self, "_cleanup_death")
	# The alert animation correponds with "tween" and works for trigger
	alert()


# Todo: Could be clearer that this
func _cleanup_death(obj, key):
	print("DEBUG: Performing object removal action")
	if (key == ":modulate"):
		get_parent().remove_child(self)
		queue_free()

# Comes from the combat module
func battle_won():
	$WorldTurnTimer.reset()

func enable_movement():
	$WorldObjectEmitter.became_movable()
	$WorldSlotMover.activate()

func set_description(desc_string):
	$Description.text = desc_string

func get_description():
	return description
