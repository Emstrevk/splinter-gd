extends Node2D

class_name WorldObjectCombatModule

signal battle_won


func _ready():
	self.name = "WorldObjectCombatModule"
	connect("battle_won", get_parent(), "battle_won")

# Todo: Self contain all combat data
func fight(enemy_object):
	print("Objects are fighting")
	if (enemy_object.allegiance != get_parent().allegiance):
		if (get_parent().battle_power > enemy_object.battle_power):
			# Still very primitive, but we'll rework this once it's an actual feature
			enemy_object.destroy()
			emit_signal("battle_won")
