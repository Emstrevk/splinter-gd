extends Node2D

class_name ObjectInteraction

signal triggered

var _world_object_parent

func trigger(object):
	if (_world_object_parent == null):
		print("ERROR: Tried to trigger interaction " + name + " with null object parent")
		get_tree().quit()
	_overload_trigger(object)
	
func _overload_trigger(object):
	print("ERROR: Tried to call base class interaction trigger (does nothing)")

func set_world_object_parent(world_object):
	_world_object_parent = world_object
