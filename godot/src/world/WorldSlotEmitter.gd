extends Node2D

class_name WorldSlotEmitter

var listener = "world_slot_listeners"

# TODO: Typing broken
onready var world_slot = get_parent()

func _ready() -> void:
	self.name = "WorldSlotEmitter"

func clicked():
	get_tree().call_group(listener, "clicked", world_slot)

func hovered():
	get_tree().call_group(listener, "hovered", world_slot)
	
