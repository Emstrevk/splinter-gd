extends Node2D

class_name WorldObjectEmitter

# Emitters should always only concern themselves with parent object

## Sister class: WorldObjectListener (Should always implement same methods)

var listener = "world_object_listeners"

# TODO: TYped when godot gets it shit together
onready var world_object = get_parent()

var _enabled = true

func _ready() -> void:
	self.name = "WorldObjectEmitter"

func disable():
	print("Disabld")
	_enabled = false

func clicked():
	if (_enabled):
		get_tree().call_group(listener, "clicked", world_object)
	else:
		print("Emitter disabled, not emitting click()")
		
func spawned():
	get_tree().call_group(listener, "spawned", world_object)
		
func unclicked():
	if (_enabled):
		get_tree().call_group(listener, "unclicked", world_object)
	else:
		print("Emitter disabled, not emitting unclick()")

func hovered():
	if (_enabled):
		get_tree().call_group(listener, "hovered", world_object)
	
func began_moving():
	if (_enabled):
		get_tree().call_group(listener, "began_moving", world_object)
	
func finished_moving():
	if (_enabled):
		get_tree().call_group(listener, "finished_moving", world_object)
	
func died():
	if (_enabled):
		get_tree().call_group(listener, "died", world_object)
	
func became_movable():
	if (_enabled):
		get_tree().call_group(listener, "became_movable", world_object)
