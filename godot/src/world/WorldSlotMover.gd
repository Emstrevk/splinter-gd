extends Node2D

# Is a worldOBJECTmover, change name
# TODO: Typing broken
class_name WorldSlotMover

signal object_moved

var moving_object

var movable_slots = []

onready var parent  = get_parent()

var _active = false

func _ready() -> void:
	var wo_ls = WorldSlotListener.new()
	wo_ls.name = "WorldSlotListener"
	self.add_child(wo_ls)

	var wo_lis = WorldObjectListener.new()
	wo_lis.name = "WorldObjectListener"
	self.add_child(wo_lis)
	
	var wt_e = WorldTurnEmitter.new()
	wt_e.name = "WorldTurnEmitter"
	self.add_child(wt_e)

func activate() -> void:
	_active = true

func deactivate() -> void:
	_active = false

func world_slot_clicked(slot ) -> void:
	if (_active):
		if (slot.is_interactable()):
			parent.move_to(slot)
			$WorldTurnEmitter.turn_taken()

# TODO: Circular prevents typing
func world_object_became_movable(world_object) -> void:
	if (world_object != get_parent()):
		deactivate()
