extends Node2D

# Should probably be retired
# THe sorting was the only thing needed apart from group reference
# Sorting is retired since isometric grid has enough space to avoid bad placement
# Note also that grids are sorted so that children of objects have their render order 
class_name WorldObjectContainer

func _ready() -> void:
	self.name = "WorldObjectContainer"

class CoordSorter:
	static func sort(woa, wob):
		var c1x = woa.current_slot.coordinates.x
		var c1y = woa.current_slot.coordinates.y
		var c2x = wob.current_slot.coordinates.x
		var c2y = wob.current_slot.coordinates.y
		if c1x < c2x:
			return true
		elif c1x == c2x:
			if c1y < c2y:
				return true
		return false

func sort():
	var sorted = get_children()
	var sorted_safe = []

	for wo in sorted:
		if (wo.current_slot != null):
			sorted_safe.append(wo)

	sorted_safe.sort_custom(CoordSorter, "sort")
	var index = 0
	for wo in sorted_safe:
		move_child(wo, index)
		index +=1
