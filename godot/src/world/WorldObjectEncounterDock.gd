extends Node2D

class_name WorldObjectEncounterDock

# NONE should be first one and default, add if-clause for this 
export(int,"1","2","3","4") var _trigger_after_turns

# TODO: Typing broken
onready var _object_parent = get_parent()

# Place any related encounter here
# Using the trigger after turns var enables a turn timer to call encounter start
# Engaging manually requires addition of that inspector action

func _ready():
	
	self.name = "WorldObjectEncounterDock"
	
	self.add_child(WorldTurnTimer.new())
	self.add_child(Services.new())
	self.add_child(EncounterListener.new())
	self.add_child(EncounterEmitter.new())
	
	yield( get_tree().create_timer(0.2), "timeout" )
	$WorldTurnTimer.hide()
	if (get_encounter() != null and _trigger_after_turns != null):
		$WorldTurnTimer.show()
		$WorldTurnTimer.set_timer(_trigger_after_turns + 1)
		$WorldTurnTimer.connect("turn_timer_ended", self, "start_encounter", [])

# Since we have potentially many children get the first one to exhibit encounter-like behaviour
func get_encounter() -> Encounter:
	for child in get_children():
		if child.has_method("export_units"):
			return child
	print("ERROR: Encounter dock has no encounter for get_encounter")
	return null

# TOdo: Emitter
func start_encounter():
	
	# We have to drop the encounter because we are
	#	part of world while battle takes place in battle screen
	# THis means we need to tell the encounter to come back
	# 	after the battle, like the unit container does
	
	get_parent().alert()
	if (get_encounter() != null):
		get_encounter().connect("return_to_world", self, "encounter_return_signal")
		get_encounter().trigger()
	else:
		print("ERROR: Attempted to start_encounter with no encounter present")
		
# Signal-based callback
# Since encounters are detached on battle start
#	there is no way to take the general defeated event
#	and check for a match. Instead we connect directly on trigger.
func encounter_return_signal(encounter : Encounter):
	add_child(encounter)
	
# From listener
func encounter_defeated(encounter : Encounter) -> void:
	if encounter == get_encounter():
		print("DEBUG: detected defeated encounter, destroying containing world object")
		var wo_parent = get_parent() 
		wo_parent.destroy()
