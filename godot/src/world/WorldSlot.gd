extends Node2D

# Not carefully reviewed yet. Seems it could use some separation of graphics though. And neighbour stuff.
class_name WorldSlot

var unhovered_body
onready var hovered_body = preload("res://graphics/worldslot/WorldTileLighter.png")

var OBJECT_OFFSET_X = 920
var OBJECT_OFFSET_Y = 200

var coordinates = Vector2(0,0)

# TODO: Circular reference, simplify reference
var world_object_occupant

var _hoverable = false

func _ready() -> void:
	
	var sprite = Sprite.new()
	sprite.texture = load("res://graphics/worldslot/WorldTile.png")
	sprite.name = "Sprite"
	unhovered_body = sprite.texture
	self.add_child(sprite)
	
	var selector = TargetIndicator.new()
	selector.name = "Selector"
	selector.texture = load("res://graphics/worldslot/WorldTileHighlight.png")
	self.add_child(selector)
	
	self.add_child(WorldSlotEmitter.new())
	self.add_child(WorldObjectListener.new())
	
	# TODO: Is it used?
	var label = Label.new()
	label.name = "Info"
	self.add_child(label)
	
	var object_dock = Node2D.new()
	object_dock.name = "ObjectDock"
	self.add_child(object_dock)
	
	var mousein = MouseInput.new()
	self.add_child(mousein)

	
# Signal emitted when objects become ready - this call handles initial placement
func world_object_spawned(world_object : WorldObject):
	print("TODO: world object coordinate placement is wonky and bug prone")
	if (world_object.get_starting_position() == coordinates):
		world_object.place(self)
	#else:
		#print("DEBUG: " + str(world_object.get_starting_position()) + " is not " + str(coordinates))

func set_lightness(amount):
	$Sprite.set_modulate(Color(amount,amount,amount,1))

func highlight():
	$Selector.show()
	_hoverable = true

func de_highlight():
	$Selector.hide()
	_hoverable = false

func clicked():
	# Check to avoid overriding object clicks
	if (world_object_occupant == null):
		$WorldSlotEmitter.clicked()

func hovered():
	if (_hoverable):
		$Sprite.texture = hovered_body

func get_neighbours():
	var neighbours = []

	var grid = get_parent().get_parent()

	var east : WorldSlot
	var west : WorldSlot
	var north : WorldSlot
	var south : WorldSlot

	north = grid.get_slot_at(coordinates + Vector2(0,-1))

	south = grid.get_slot_at(coordinates + Vector2(0,1))

	east = grid.get_slot_at(coordinates + Vector2(1,0))

	west = grid.get_slot_at(coordinates + Vector2(-1,0))

	if (east != null):
		neighbours.append(east)

	if (west != null):
		neighbours.append(west)

	if (north != null):
		neighbours.append(north)

	if (south != null):
		neighbours.append(south)


	return neighbours

func is_close(slot : WorldSlot) -> bool:
	return get_neighbours().has(slot)

### Moving functions, could easily be ported to child object

func highlight_if_near(object : WorldObject):
	de_highlight()
	if (object != null):
		if (is_close(object.current_slot)):
			highlight()

func world_object_became_movable(object : WorldObject):
	highlight_if_near(object)

func world_object_began_moving(object : WorldObject):
	highlight_if_near(object)

func world_object_died(object : WorldObject):
	highlight_if_near(object)

func set_coordinates(vector):
	coordinates = vector
	$Info.text = str(vector.x) + "," + str(vector.y)

####

func is_interactable():
	return _hoverable

func unhovered() -> void:
	$Sprite.texture = unhovered_body

func to_string() -> String:
	return "SLOT[" + str(coordinates.x) + "," + str(coordinates.y) + "]"

func get_object_position() -> Vector2:
	return Vector2(self.position.x + OBJECT_OFFSET_X, self.position.y + OBJECT_OFFSET_Y)
