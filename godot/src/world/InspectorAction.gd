extends Node2D

# More or less a request but likely to branch out eventually 

var _world_object_parent : WorldObject = null

export(String) var title

func _ready():
	if (title == null):
		print ("ERROR: Inspector action " + name + " is missing title")
		get_tree().quit()
		
func resolve():
	if (_world_object_parent == null):
		print("ERROR: Inspector action " + title + " has no world object reference.")
		get_tree().quit()
		
	_overload_resolve()
	
func _overload_resolve():
	print("ERROR: Tried to use base inspector action object")
	
func set_world_object_parent(world_object : WorldObject):
	_world_object_parent = world_object
