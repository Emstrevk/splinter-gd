extends Node2D

class_name WorldTurnEmitter

var listener = "world_turn_listeners"

func turn_taken() -> void:
	get_tree().call_group(listener, "turn_taken")
