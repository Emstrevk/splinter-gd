extends Node2D

class_name FPSCounter

const TIMER_LIMIT = 2.0
var timer = 0.0

func _ready() -> void:
	var label = Label.new()
	# TODO: Can't really fuck around with this 
	# Since it requires a font. 
	# We should probably make our own label impl
	label.name = "Label"
	label.set_size(Vector2(79, 46))
	self.add_child(label)

func _process(delta):
	timer += delta
	if timer > TIMER_LIMIT: # Prints every 2 seconds
		timer = 0.0
		$Label.text = "fps: " + str(Engine.get_frames_per_second())
