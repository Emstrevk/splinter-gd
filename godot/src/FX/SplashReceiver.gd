extends Node2D

# Where is caller.gd?
class_name SplashReceiver

# Receive node that displays effects on parent (aka itself) based on event calls
# Paired with SplashCaller to find its way home

var bubble_scene = preload("res://scenes/effects/StaticSplash.tscn")
var splash_scene = preload("res://scenes/effects/SimpleSplash.tscn")

func _ready():
	add_to_group("splash_receivers")

func say(target_node : Node2D, message: String) -> void:
	if (target_node == get_parent()):
		var splash: StaticSplash = bubble_scene.instance()
		get_parent().add_child(splash)
		splash.position = Vector2(0,0)
		splash.label(message)

func drop_splash(target_node : Node2D, preloaded_texture : Texture) -> void:
	if (target_node == get_parent()):
		var splash = splash_scene.instance()
		splash.scale = Vector2(splash.scale.x / 2, splash.scale.y / 2)
		get_parent().add_child(splash)
		splash.position = Vector2(0,0)
		splash.fire(preloaded_texture)
