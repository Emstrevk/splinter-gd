extends Node2D

class_name AbilityEmitter

var _listener = "ability_listeners"

func selected():
	get_tree().call_group(_listener, "selected", get_parent())

func started():
	get_tree().call_group(_listener, "started", get_parent())
	
func failed():
	get_tree().call_group(_listener, "failed", get_parent())
	
func succeeded():
	get_tree().call_group(_listener, "succeeded", get_parent())
	
func new_targets():
	get_tree().call_group(_listener, "new_targets", get_parent())
