extends Ability

class_name StrikeAbility

func _ready() -> void:
	._ready()
	var active_effect = DamageTargetsActiveEffect.new()
	active_effect.base_damage = 50.0
	.get_node("ActiveEffects").add_child(active_effect)
	var target_fetcher = MainTargetOnlyTargetFetcher.new()
	active_effect.add_child(target_fetcher)
