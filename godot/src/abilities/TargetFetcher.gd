extends Node2D

class_name TargetFetcher

signal target_fetcher_ready

# Always overload
func fetch_targets(caster_slot, main_target):
	return []

func _ready() -> void:
	
	var services = Services.new()
	services.name = "Services"
	self.add_child(services)
	
	if (get_parent().has_method("target_fetcher_ready")):
		get_parent().target_fetcher_ready(self)
