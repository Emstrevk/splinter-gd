extends Node2D

class_name Ability

onready var _effect_delay = Timer.new()

var _caster_slot : Slot
var _targets = []

var _awaited_effects = 0

var _listening_for_targets = false

# Husk class, not to be overridden

func execute(main_target : Slot) -> void:
	# TODO: get_targets of first effect should determine if success
	# E.g. if we damage and expose shit won't fly if damage fails

	if _caster_slot == null:
		print("ERROR: Attempted to execute ability with no caster")
		return

	_caster_slot.animate_strike()

	for effect in $ActiveEffects.get_children():
		effect.set_default_icon($Icon.texture)
		
	var targets = get_targets()

	if (targets != []):
		# Smack out the effect splashes
		for target in targets:
			
			var tTarget : Slot = target
			
			if (tTarget != targets[0]):
				_effect_delay.start()
				yield(_effect_delay, "timeout")
			var unique_splash = null
			
			for effect in $ActiveEffects.get_children():
				var tEffect : ActiveEffect = effect
				if (tTarget in tEffect.get_targets(_caster_slot, main_target)):
					# Don't splash targets twice with the same icon
					if (unique_splash != tEffect.get_icon()):
						unique_splash = tEffect.get_icon()
						tEffect.splash(tTarget)

		# Run apply for every effect
		for effect in $ActiveEffects.get_children():
			var tEffect : ActiveEffect = effect
			if (len($ActiveEffects.get_children()) > 1):
				_effect_delay.start()
				yield(_effect_delay, "timeout")
			tEffect.apply(_caster_slot, main_target)

		$AbilityEmitter.succeeded()
	else:
		$AbilityEmitter.failed()

	_listening_for_targets = false
	_targets = []

func select() -> void:
	_listening_for_targets = true
	_targets = []
	$AbilityEmitter.selected()

# Avoids getting the same target twice by filtering duplicates
func get_targets() -> Array:
	var unique_targets = []

	for target in _targets:
		var tTarget : Slot = target # Example of explicit typing in list loop
		if (!tTarget in unique_targets):
			unique_targets.append(tTarget)

	return unique_targets

# Used with things like AI to ready targets without user input
func target_manually(main_target : Slot):
	_fetch_targets(main_target)

func _ready():

	_effect_delay.set_wait_time(0.3)
	_effect_delay.set_one_shot(true)
	
	
	# TODO: Starting here with "build via script"
	# I think a lot of bullshit scene structures can be eliminated using this
	
	var slot_listener = SlotListener.new()
	slot_listener.name = "SlotListener"
	self.add_child(slot_listener)
	
	var active_effects = Node2D.new()
	active_effects.name = "ActiveEffects"
	self.add_child(active_effects)
	
	var passive_effects = Node2D.new()
	passive_effects.name = "PassiveEffects"
	self.add_child(passive_effects)
	
	var emitter : AbilityEmitter = AbilityEmitter.new()
	emitter.name = "AbilityEmitter"
	self.add_child(emitter)
	
	var splasher : SplashCaller = SplashCaller.new()
	splasher.name = "SplashCaller"
	self.add_child(splasher)
	
	var icon : Sprite = Sprite.new()
	icon.name = "Icon"
	icon.texture = load("res://graphics/icons/debugIcon.png")
	icon.hide()
	self.add_child(icon)
	
	var tooltip : Label = Label.new()
	tooltip.name = "ToolTip"
	tooltip.text = "Default tooltip: This ability has no tooltip setting"
	tooltip.hide()
	self.add_child(tooltip)
	
	self.add_child(_effect_delay)

func splash(target_slot : Slot) -> void:
	$SplashCaller.drop_splash(target_slot, $Icon.texture)

func get_tooltip() -> String:
	return $ToolTip.text

func get_icon() -> Texture:
	return $Icon.texture

# From listener
func slot_selected(slot : Slot) -> void:
	_caster_slot = slot

func slot_hovered(slot : Slot) -> void:
	if _listening_for_targets:
		_fetch_targets(slot)

func slot_unhovered(slot : Slot) -> void:
	if _listening_for_targets:
		_targets = []
		$AbilityEmitter.new_targets()

func slot_clicked(slot : Slot) -> void:
	if _listening_for_targets:
		execute(slot)

# Children of active effects are target fetchers, by convention
func _fetch_targets(main_target : Slot) -> void:
	print("DEBUG: Fetching targets for " + self.name)
	_targets = []
	
	if (len($ActiveEffects.get_children()) == 0):
		print("ERROR: Ability has no active effects")
	
	for effect in $ActiveEffects.get_children():
		var tEffect : ActiveEffect = effect
		_targets += tEffect.get_targets(_caster_slot, main_target)
	$AbilityEmitter.new_targets()
	
func add_active_effect(effect : ActiveEffect) -> void:
	$ActiveEffects.add_child(effect)

