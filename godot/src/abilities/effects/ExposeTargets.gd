extends "res://src/abilities/ActiveEffect.gd"

# Use parent class name

# Main overload
func _overload_apply(caster, main_target) -> void:
	for target in get_targets(caster, main_target):
		target.expose()
