extends "res://src/abilities/ActiveEffect.gd"

# Use parent class name

class_name SelfExposeConditionalActiveEffect

# Main overload
func _overload_apply(caster, main_target) -> void:
	if (is_overextending(caster, main_target)):
		caster.expose()
