extends ActiveEffect

class_name DamageTargetsActiveEffect

export var base_damage : float

func _ready() -> void:
	._ready()

# Main overload
func _overload_apply(caster, main_target) -> void:
	var targets = get_targets(caster, main_target)
	
	for target in targets:
		target.damage_unit(base_damage)
