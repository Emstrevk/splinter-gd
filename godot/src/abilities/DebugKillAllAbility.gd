extends Ability

class_name DebugKillAllAbility

# So how do we work this?

func _ready() -> void:
	._ready()
	print("DEBUG: Override init called")
	var some_effect = DamageTargetsActiveEffect.new()
	some_effect.base_damage = 50.0
	
	.get_node("ActiveEffects").add_child(some_effect)
	var target_fetcher = AllEnemiesTargetFetcher.new()
	target_fetcher.name = "ALL TARGETS YO"
	some_effect.add_child(target_fetcher)
