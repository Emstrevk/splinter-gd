extends Node2D

class_name ActiveEffect

var _target_fetcher = null
onready var delay_timer : Timer = Timer.new()

signal application_finished

var _damage : int = 0
var _latest_targets = []

# Abstract class, overload children

# Main overload
func apply(caster, main_target) -> void:
	_overload_apply(caster, main_target)

func get_targets(caster, main_target):
	if (_target_fetcher == null):
		print("ERROR: Active effect has no target fetcher!")
	return _target_fetcher.fetch_targets(caster, main_target)

func _overload_apply(caster, main_target) -> void:
	print("ERROR: Attempted to use abstract activeEffect for ability")

func target_fetcher_ready(target_fetcher: TargetFetcher) -> void:
	#print("UNIMPLEMENTED: Active effect only has one target fetcher")
	#print("DEBUG: Target Fetcher: " + target_fetcher.name + " ready")
	_target_fetcher = target_fetcher

func _ready() -> void:

	# Should use tree timer instead, removing vars as well
	delay_timer.set_one_shot(true)
	delay_timer.set_wait_time(0.3)
	add_child(delay_timer)
	
	var icon = Sprite.new()
	icon.texture = load("res://graphics/icons/debugIcon.png")
	icon.name = "Icon"
	add_child(icon)
	icon.hide()
	
	var slot_groups = SlotStatusGroups.new()
	slot_groups.name = "SlotStatusGroups"
	add_child(slot_groups)
	
	var services = Services.new()
	services.name = "Services"
	add_child(services)
	
	var splash_caller = SplashCaller.new()
	splash_caller.name = "SplashCaller"
	add_child(splash_caller)

func set_default_icon(icon : Texture) -> void:
	if ($Icon.texture == null):
		$Icon.texture = icon

func get_icon():
	return $Icon.texture

func splash(target_slot : Slot) -> void:
	$SplashCaller.drop_splash(target_slot, $Icon.texture)

func is_overextending(caster : Slot, target : Slot):
	var target_protected = target.slot_status().is_normal(target)
	var caster_unprotected = caster.slot_status().is_normal(caster)
	return target_protected and caster_unprotected and caster.get_parent() != target.get_parent()
