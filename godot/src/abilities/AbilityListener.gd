extends Node2D

class_name AbilityListener

signal selected
signal started
signal failed
signal succeeded
signal new_targets

func _ready():
	safe_connect("selected", "ability_selected")
	safe_connect("started", "ability_started")
	safe_connect("failed", "ability_failed")
	safe_connect("succeeded", "ability_succeeded")
	safe_connect("new_targets", "ability_new_targets")
	
	add_to_group("ability_listeners")

func safe_connect(listener_signal, parent_function):
	if (get_parent().has_method(parent_function)):
		connect(listener_signal, get_parent(), parent_function)

func selected(ability):
	emit_signal("selected", ability)
	
func started(ability):
	emit_signal("started", ability)
	
func failed(ability):
	emit_signal("failed", ability)
	
func succeeded(ability):
	emit_signal("succeeded", ability)
	
func new_targets(ability):
	emit_signal("new_targets", ability)
