extends TargetFetcher

class_name AllEnemiesTargetFetcher

func _ready() -> void:
	._ready()
	var services = Services.new()
	services.name = "Services"
	self.add_child(services)

func fetch_targets(caster : Slot, main_target : Slot):
	# TODO: "SlotAllegienceGroups" helper instead
	print("DEBUG: AllEnemiesTargetFetcher called")
	var bs : BattleService = $Services.BATTLE
	return bs.get_all_allied_slots(main_target)
