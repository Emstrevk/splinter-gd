extends TargetFetcher

class_name AllExposedTargetFetcher

var slot_groups : SlotStatusGroups

func _ready() -> void:
	._ready()
	slot_groups = SlotStatusGroups.new()
	add_child(slot_groups)
	

func fetch_targets(caster, main_target):
	if (main_target.slot_status().is_exposed(main_target)):
		return slot_groups.get_all_exposed()
	return []
