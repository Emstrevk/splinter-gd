extends TargetFetcher

class_name MainTargetOnlyTargetFetcher

func fetch_targets(caster, main_target):
	if (main_target.get_unit() != null):
		return [main_target]
	else:
		return []
