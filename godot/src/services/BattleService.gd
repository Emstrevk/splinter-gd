extends Node2D

# Should be "BattleField" or "BattleScene"
class_name BattleService

var _selected_slot

var _turn_switch_time = 0.2

var _current_slot_row = 0

onready var _turn_timer = Timer.new()
onready var _turn_switch_timer = Timer.new()

onready var _turn_switcher = [$PlayerSlots, $EnemySlots]

var background_decoration = preload("res://scenes/battle/BattleTiles.tscn")

func _ready() -> void:
	add_child(background_decoration.instance())
	
	var player_slots = SlotRack.new()
	player_slots.name = "PlayerSlots"
	player_slots.position = Vector2(450,350)
	player_slots.invert_order = true
	add_child(player_slots)
	var enemy_slots = SlotRack.new()
	enemy_slots.position = Vector2(1100, 250)
	enemy_slots.name = "EnemySlots"
	add_child(enemy_slots)
	
	add_child(AbilityListener.new())
	add_child(BattleEmitter.new())
	add_child(Services.new())
	add_child(EncounterListener.new())
	
	var background = Sprite.new()
	background.texture = load("res://background.png")
	background.name = "Background"
	add_child(background)
	move_child(background, 0)
	
	
	var ebox = Node2D.new()
	ebox.name = "EncounterBox"
	add_child(ebox)

# SHould be handled using encounter.trigger() listener instead of called directly
func begin_battle(encounter : Encounter):

	_turn_timer.set_wait_time(_turn_switch_time)
	_turn_timer.set_one_shot(true)
	self.add_child(_turn_timer)

	# Encounter moving, not sure if a good way to go about it
	# IF we're to move the encounter to child of this, do it all in the same place for less confusion (here)
	$EncounterBox.add_child(encounter)
	var encounter_container = $EncounterBox.get_child(0)

	if (encounter_container == null):
		print("ERROR: Tried to start battle with no encounter added")
		get_tree().quit()

	# Best keep this here, if slots themselves listen there is risk of race condition
	$EnemySlots.receive_units(encounter.export_units())

	# Should be group reference instead
	var player_container = $Services.PLAYER.get_node("PartyContainer")

	if (player_container == null):
		print("ERROR: Tried to start battle with no player party")
		get_tree().quit()


	$PlayerSlots.receive_units(player_container.export_units())

	# Could be internal to receive units, but it's also nice to have battle setup in here
	$PlayerSlots.create_tank()
	$EnemySlots.create_tank()

	# Needed not only for effect but because calling "onready" for all damn slots and abilities is hell
	_turn_timer.start()
	yield(_turn_timer, "timeout")

	$EnemySlots.set_enemy_coloring()

	new_turn()


# Selects a new slot in order. Will select by row/unit rather than 1:1 team ratio
func select_next_slot():
	_selected_slot = null
	while _selected_slot == null:
		_selected_slot = _turn_switcher[0].iterate()
		_turn_switcher.invert()

	_selected_slot.select()

	# SInce we invert after finding a slot the "currently selected" slotrack is the second one
	if (_turn_switcher[1] == $EnemySlots):
		_selected_slot.get_unit().execute_ai()

func new_turn():

	# Should really be called when we call new_turn() in actuality; this will trigger on first turn start
	$BattleEmitter.turn_ended()

	_turn_timer.set_wait_time(_turn_switch_time)

	# Create new tanks if missing
	# Note that a safety check for get_tank 
	# happens internally as well, but 
	# this check is to place the timer-wait() accordingly
	if $PlayerSlots.get_tank() == null:
		_turn_timer.start()
		yield(_turn_timer, "timeout")
		$PlayerSlots.create_tank()
	if $EnemySlots.get_tank() == null:
		_turn_timer.start()
		yield(_turn_timer, "timeout")
		$EnemySlots.create_tank()

	_turn_timer.set_wait_time(_turn_switch_time * 2)
	_turn_timer.start()
	yield(_turn_timer, "timeout")

	# This COULD rely on listeners, but it's neat to have the central logic here
	if ($PlayerSlots.count_units() > 0 && $EnemySlots.count_units() > 0):
		select_next_slot()
	elif ($PlayerSlots.count_units() == 0):
		end_defeat()
	else:
		# There was a race condition in here with
		# encounter's return-to-world
		# It should be resolved once that relies on listeners
		# properly
		_turn_timer.set_wait_time(_turn_switch_time * 2)
		_turn_timer.start()
		yield(_turn_timer, "timeout")
		end_victory()

func end_defeat():
	$BattleEmitter.end_defeat()

func end_victory():
	$BattleEmitter.end_victory()
	
# Ordered extraction to be called by a management service
func extract_encounter() -> Encounter:
	var encounter : Encounter = $EncounterBox.get_child(0)
	if encounter == null:
		print("ERROR: Called extract_encounter without encounter present")
		return null
	$EncounterBox.remove_child(encounter)
	return encounter

func ability_succeeded(ability):
	new_turn()

# Should use groups instead, but remember that that needs removal/addition as well as it is dynamic
# Start by adding groups, removing calls to this, then remove
func get_all_exposed_slots():
	return $PlayerSlots.get_all_exposed() + $EnemySlots.get_all_exposed()

# Should also be group-based ideally
func get_all_allied_slots(main_target : Slot):
	if main_target in $PlayerSlots.slots:
		return $PlayerSlots.slots
	elif main_target in $EnemySlots.slots:
		return $EnemySlots.slots

# Can be done with groups but not sure about best way
# Only used for fetching tank in AI
# Racks can be in a group and keep get_tank, but that's still singleton groups which is a bit bad
# Tricky. But at least this method has to go since battleService direct call is to be removed
func get_player_rack() -> SlotRack:
	return $PlayerSlots as SlotRack	
