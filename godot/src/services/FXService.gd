extends Node2D

# Rename to cloudspawner or similar
# Perhaps ambienteffectmachine with settable sprite types
class_name FXService

# More or less dead at this point, could be a "cloud spawner" for all I care

onready var cloud_timer = Timer.new()

var start_clouds = 40
var spawned_start_clouds = 0

func _process(delta):

	if (spawned_start_clouds < start_clouds):
		spawned_start_clouds += 1
		var splash_scene = load("res://scenes/effects/AmbientEffect.tscn")
		for i in range(0,1):
			var splash: StaticSplash = splash_scene.instance()
			var y_level = rand_range(-500,500)
			var origin_x = (70 * spawned_start_clouds) -400

			splash.position = Vector2(origin_x,y_level)
			splash.scale = Vector2(rand_range(splash.scale.x, splash.scale.x * 2), rand_range(0.1, \
				splash.scale.y * 2))
			splash.modulate = Color(1,1,1,rand_range(0.015,0.2))
			add_child(splash)
			splash._move(Vector2(-400, y_level), Vector2(origin_x,y_level))
