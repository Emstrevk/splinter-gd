extends Unit 

class_name MutantUnit

func _ready() -> void:
	self.name = "Mutant"
	$ActiveAbilities.add_child(StrikeAbility.new())
	$Portrait.texture = load("res://graphics/MutantPortrait.png")	
