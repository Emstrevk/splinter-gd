extends Encounter

class_name ThreeMutantsEncounter

func _ready() -> void:
	.ready()
	$PartyContainer.add_child(MutantUnit.new())
	$PartyContainer.add_child(MutantUnit.new())
	$PartyContainer.add_child(MutantUnit.new())
