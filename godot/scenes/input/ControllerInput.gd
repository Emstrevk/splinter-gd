extends Control

# Detect standard controller events
class_name ControllerInput

signal axis_up
signal axis_down
signal axis_left
signal axis_right

signal accept_pressed

func _ready():
	safe_connect("axis_up", "controller_axis_up")
	safe_connect("axis_down", "controller_axis_down")
	safe_connect("axis_right", "controller_axis_right")
	safe_connect("axis_left", "controller_axis_left")
	
	safe_connect("accept_pressed", "controller_accept_pressed")
	
func safe_connect(listener_signal, parent_function):
	if (get_parent().has_method(parent_function)):
		connect(listener_signal, get_parent(), parent_function)	
		
func _input(event : InputEvent) -> void:
	if Input.is_action_pressed("ui_accept"):
		emit_signal("accept_pressed")
	if Input.is_action_pressed("ui_up"):
		emit_signal("axis_up")
	if Input.is_action_pressed("ui_down"):
		emit_signal("axis_down")
	if Input.is_action_pressed("ui_left"):
		emit_signal("axis_left")
	if Input.is_action_pressed("ui_right"):
		emit_signal("axis_right")
	
