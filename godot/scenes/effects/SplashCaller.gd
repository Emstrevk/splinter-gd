extends Node2D

class_name SplashCaller

var receivers = "splash_receivers"

func say(target_node, message : String) -> void:
	get_tree().call_group(receivers, "say", target_node, message)
	
func drop_splash(target_node, texture : Texture):
	get_tree().call_group(receivers, "drop_splash", target_node, texture)
