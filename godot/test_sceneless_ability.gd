extends Node

class_name SceneLessAbility

var damage_targets_packed_scene = preload("res://scenes/abilities/effects/DamageTargets.tscn")
var main_target_packed_scene = preload("res://scenes/abilities/targetFetchers/MainTargetOnly.tscn")

func _ready() -> void:
	var ability = Ability.new()
	add_child(ability)
	var dt = damage_targets_packed_scene.instance()
	ability.add_active_effect(dt)
	var target = main_target_packed_scene.instance()
	dt.add_child(target)
	
	# Tricky. You can have an "AbilityBuilder" that returns a scene
	# but you can never get properly gui-less abilities I think...

