extends Node2D

# Gamepad class to select objects/tiles for interaction
class_name WorldCursor

# 1. Should have access to the SlotGrid class methods
# - Investigate Zone.tscn structure

# 2. Calls highlight on WorldSlots but does not itself 
#	maintain any graphics
# 3. Should handle ui-specific logic, like selecting the
#	world object rather than the underlying slot if wo present
# 4. Needs to be activated/deactivated based on world logic/
#	listeners (input status)
# 5. Compare to and make generic for: 
#	a. UI buttons (button list cursor)
#	b. Battle (battle cursor)
