extends Unit

class_name ValganUnit

func _ready() -> void:
	self.name = "Valgan"
	$ActiveAbilities.add_child(DebugKillAllAbility.new())
	$Portrait.texture = load("res://graphics/ValganPortrait.png")	
	
